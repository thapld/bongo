![img.png](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)
![img_3.png](img_3.png)

## Biển số xe theo kinh dịch quẻ

![img_5.png](img_5.png)
![img_7.png](img_7.png)
![img_6.png](img_6.png)
![img_4.png](img_4.png)

### Tiêu chí 1: lấy quẻ

VD: 51F-322.89
Thường quái: 322 = (3 + 2+ 2) % 8 = 7
Hạ quái: 89 = (8 + 9) % 8 = 1
-> Quẻ 7/1 -> Quẻ Sơn tiên/ Đại súc
Nếu % 8 = 0 lấy 8

### Tiêu chí 2: Lấy hào đồng

Ví dụ: 51F-322.89
3 + 2 + 2 + 8 + 9 = 24
24 % 6 = 0 -> lấy 6
-> Quẻ biến

### Tiêu chí 3

Ví dụ: 51F-322.89
Bỏ Thượng quái. Kiểm tra hạ quái
89
8 % 8 = 0 -> 8
9 % 8 = 8
-> Quẻ 8/8

### Tiêu chí 4

Thủy: 1-6 Xanh nước biển, tím đậm, đen
Hỏa: 2-7 Đỏ, cam
Mộc: 3-8 Xanh lá cây
Kim: 4-9 Trắng, xám, ghi
Thổ: 5-10 Nâu, vàng đất
Ví dụ: 32289 màu trắng -> hành kim
Lấy 2 số cuối để tính
89 => 8/1 Địa thiên thái
Màu sắc / 6 lấy hào động
= 8 + 9 + 4 + 9 = 30 % 6 = 6

1 tiêu chí xấu là xấu

Ví dụ biển: 90B1-16812
--TC1:
(1+6+8) = 15 % 8 = 7
(1+2) = 3 % 8 = 3
-> 7/3 -> Sơn hỏa bí -> Đẹp
--TC2:
1+6+8+1+2=18 % 6 = 6 => Địa hỏa minh di -> Xấu
--TC3:
168|12 màu đỏ
1 % 8 = 1
2 % 8 = 2
-> 1/2 -> Thiên lý trạch => Đẹp
--TC4:
1 + 2 + 2 + 7 = 12 % 6 = 6
hào 6 => Thuần Càn => Đẹp
