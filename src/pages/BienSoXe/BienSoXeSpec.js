import React from 'react'
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import { colors, fonts, sizes } from '../../theme'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: sizes.padding,
    paddingHorizontal: sizes.padding,
  },
})

const BienSoXeSpec = ({ route }) => (
  <View style={styles.container}>
    <FlatList
      data={route.params.item.data}
      bounces={false}
      showsVerticalScrollIndicator={false}
      keyExtractor={(item) => item.id}
      contentContainerStyle={{
        paddingBottom: sizes.padding,
      }}
      renderItem={({ item, index }) => (
        <TouchableOpacity
          key={`${item.type}-${index}`}
          style={{
            flex: 1,
            flexDirection: 'row',
            paddingHorizontal: sizes.radius,
            backgroundColor: colors.lightPurple,
            paddingVertical: sizes.padding,
            alignItems: 'flex-start',
            justifyContent: 'space-between',
            borderRadius: sizes.radius,
            marginBottom: sizes.base,
          }}
        >
          <View style={{ flexDirection: 'row', flexShrink: 1 }}>
            <Text
              style={{ flexShrink: 1, ...fonts.h3, color: colors.white }}
              numberOfLines={2}
            >
              {item.description}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', flexShrink: 1 }}>
            <Text
              style={{ flexShrink: 1, ...fonts.h3, color: colors.white }}
              numberOfLines={2}
            >
              {item.licensePlate}
            </Text>
          </View>
        </TouchableOpacity>
      )}
    />
  </View>
)

export default BienSoXeSpec
