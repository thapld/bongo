import React, { useEffect, useState } from 'react'
import {
  ActivityIndicator,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import axios from 'axios'
import Toast from 'react-native-toast-message'
import { Feather } from '@expo/vector-icons'
import { colors, fonts, sizes } from '../../theme'
import api from '../../constants/api'
import FormInput from '../../components/FormInput'
import utils from '../../utils'
import IconLabel from '../../components/IconLabel'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: sizes.padding,
    paddingHorizontal: sizes.padding,
  },
})

const BienSoxeDistrict = ({ route }) => {
  const [districtList, setDistrictList] = useState([])
  const [group, setGroup] = useState([])
  const [loading, setLoading] = useState(false)
  const [districtListSearch, setdistrictListSearch] = useState([])

  const onSearch = (text) => {
    if (!text) {
      setdistrictListSearch(districtListSearch)
    } else {
      setdistrictListSearch(
        districtList.filter(
          (item) => item.licensePlate.toLowerCase().includes(text.toLowerCase())
            || item.description.toLowerCase().includes(text.toLowerCase())
            || utils
              .removeAccents(item.description.toLowerCase())
              .includes(text.toLowerCase()),
        ),
      )
    }
  }

  useEffect(() => {
    const groupx = districtListSearch.reduce((rv, x) => {
      // eslint-disable-next-line no-param-reassign
      (rv[x.type] = rv[x.type] || []).push(x)
      return rv
    }, {})
    const sectionList = Object.keys(groupx).map((item) => ({
      title: groupx[item][0].title,
      data: groupx[item],
    }))
    setGroup(sectionList)
  }, [districtListSearch])

  useEffect(() => {
    const { adminCode } = route.params
    if (adminCode) {
      setLoading(true)
      axios
        .get(`${api.baseUrl}/bsx?provinceCode=${adminCode}`)
        .then((value) => {
          if (value && value.data && value.data.data) {
            setDistrictList(value.data.data)
            setdistrictListSearch(value.data.data)
          }
          setLoading(false)
        })
        .catch((reason) => {
          setLoading(false)
          Toast.show({
            type: 'error',
            text1: reason.message,
          })
        })
    }
  }, [])

  return (
    <View style={styles.container}>
      <FormInput
        placeholder="Tìm theo địa phương hoặc theo biển số"
        onChange={(text) => onSearch(text)}
        containerStyle={{
          paddingBottom: sizes.padding,
        }}
      />
      {loading ? (
        <ActivityIndicator color={colors.lightPurple} />
      ) : (
        <SectionList
          sections={group}
          bounces={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.id}
          renderSectionHeader={({ section }) => (
            <View
              style={{
                paddingBottom: sizes.base,
              }}
            >
              <Text
                style={{
                  ...fonts.h3,
                  color: colors.gray,
                }}
              >
                {section.title}
              </Text>
            </View>
          )}
          contentContainerStyle={{
            paddingBottom: sizes.padding,
          }}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              key={`${item.type}-${index}`}
              style={{
                flex: 1,
                flexDirection: 'row',
                paddingHorizontal: sizes.radius,
                backgroundColor: colors.lightPurple,
                paddingVertical: sizes.padding,
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                borderRadius: sizes.radius,
                marginBottom: sizes.base,
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  minWidth: 200,
                }}
              >
                <Text style={{ ...fonts.h3, color: colors.white }}>
                  {item.description}
                </Text>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                  }}
                >
                  <IconLabel
                    imageType={false}
                    labelStyle={{
                      color: colors.white,
                    }}
                    icon={
                      <Feather name="phone" size={16} color={colors.blue} />
                    }
                    label={item.districtCode}
                    containerStyle={{
                      paddingHorizontal: 0,
                    }}
                  />
                </View>
              </View>
              <View style={{ flexDirection: 'row', flexShrink: 1 }}>
                <Text
                  style={{ flexShrink: 1, ...fonts.h3, color: colors.white }}
                  numberOfLines={2}
                >
                  {item.licensePlate}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      )}
      <Toast />
    </View>
  )
}

export default BienSoxeDistrict
