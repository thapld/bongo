import React, { useEffect, useState } from 'react'
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  useWindowDimensions,
  View,
} from 'react-native'
import axios from 'axios'
import Toast from 'react-native-toast-message'
import { Feather } from '@expo/vector-icons'
import { SceneMap, TabBar, TabView } from 'react-native-tab-view'
import { colors, fonts, sizes } from '../../theme'
import api from '../../constants/api'
import IconLabel from '../../components/IconLabel'
import FormInput from '../../components/FormInput'
import utils from '../../utils'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    paddingHorizontal: sizes.padding,
  },
})

const BienSoXe = ({ navigation }) => {
  const [provinceList, setProvinceList] = useState([])
  const [specList, setSpecList] = useState([])
  const [provinceListSearch, setProvinceListSearch] = useState([])
  const [loading, setLoading] = useState(true)
  const [loadingSpec, setLoadingSpec] = useState(true)

  const layout = useWindowDimensions()

  const [index, setIndex] = React.useState(0)
  const [routes] = React.useState([
    { key: 'first', title: 'Biển thường' },
    { key: 'second', title: 'Biển đặc biệt' },
  ])

  const onSearch = (text) => {
    if (!text) {
      setProvinceListSearch(provinceList)
    } else {
      setProvinceListSearch(
        provinceList.filter(
          (item) => item.licensePlate.toLowerCase().includes(text.toLowerCase())
            || item.name.toLowerCase().includes(text)
            || utils
              .removeAccents(item.name.toLowerCase())
              .includes(text.toLowerCase()),
        ),
      )
    }
  }

  const FirstRoute = () => (
    <View style={styles.container}>
      <FormInput
        placeholder="Tìm theo tỉnh hoặc theo biển số"
        onChange={(text) => onSearch(text)}
        containerStyle={{
          paddingBottom: sizes.padding,
        }}
      />
      {loading ? (
        <ActivityIndicator color={colors.lightPurple} />
      ) : (
        <FlatList
          data={provinceListSearch}
          bounces={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.adminCode}
          contentContainerStyle={{
            paddingBottom: sizes.padding,
          }}
          renderItem={({ item, indexx }) => (
            <TouchableOpacity
              key={`${item.adminCode}-${indexx}`}
              onPress={() => navigation.navigate('BienSoXeDistrict', {
                adminCode: item.adminCode,
                name: item.name,
              })}
              style={{
                flex: 1,
                flexDirection: 'row',
                paddingHorizontal: sizes.radius,
                backgroundColor: colors.lightPurple,
                paddingVertical: sizes.padding,
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                borderRadius: sizes.radius,
                marginBottom: sizes.base,
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                }}
              >
                <Text style={{ ...fonts.h3, color: colors.white }}>
                  {item.name}
                </Text>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                  }}
                >
                  <IconLabel
                    imageType={false}
                    labelStyle={{
                      color: colors.white,
                    }}
                    icon={
                      <Feather name="phone" size={16} color={colors.blue} />
                    }
                    label={item.mobileCode}
                    containerStyle={{
                      paddingHorizontal: 0,
                    }}
                  />
                  <IconLabel
                    imageType={false}
                    labelStyle={{
                      color: colors.white,
                    }}
                    icon={
                      <Feather name="home" size={16} color={colors.green} />
                    }
                    label={item.adminCode}
                    containerStyle={{
                      marginLeft: sizes.base,
                      paddingHorizontal: 0,
                    }}
                  />
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  flexShrink: 1,
                  marginLeft: sizes.padding,
                  flex: 1,
                  justifyContent: 'flex-end',
                }}
              >
                <Text
                  style={{ flexShrink: 1, ...fonts.h3, color: colors.white }}
                  numberOfLines={2}
                >
                  {item.licensePlate}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      )}
      <Toast />
    </View>
  )

  const SecondRoute = () => (
    <View style={[styles.container, { paddingTop: sizes.padding }]}>
      {loadingSpec ? (
        <ActivityIndicator color={colors.lightPurple} />
      ) : (
        <FlatList
          data={specList}
          bounces={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.title}
          contentContainerStyle={{
            paddingBottom: sizes.padding,
          }}
          numColumns={2}
          renderItem={({ item, indexx }) => (
            <TouchableOpacity
              key={`${item.title}-${indexx}`}
              onPress={() => navigation.navigate('BienSoXeSpec', { item })}
              style={{
                flex: 1,
                flexDirection: 'row',
                paddingHorizontal: sizes.radius,
                backgroundColor: colors.lightPurple,
                paddingVertical: sizes.padding,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: sizes.radius,
                marginBottom: sizes.base,
                marginLeft: sizes.base,
              }}
            >
              <Text style={{ ...fonts.body4, color: colors.white }}>
                {item.title}
              </Text>
            </TouchableOpacity>
          )}
        />
      )}
    </View>
  )

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  })

  const getProvinceList = () => {
    axios
      .get(`${api.baseUrl}/bsx/province`)
      .then((value) => {
        if (value.data && value.data.data && value.data.data.length > 0) {
          setProvinceList(value.data.data)
          setProvinceListSearch(value.data.data)
        }
        setLoading(false)
      })
      .catch((reason) => {
        console.log(reason)
        setLoading(false)
        Toast.show({
          type: 'error',
          text1: reason.message,
        })
      })
  }

  const getSpecList = () => {
    axios
      .get(`${api.baseUrl}/bsx?type=1`)
      .then((value) => {
        if (value.data && value.data.data && value.data.data.length > 0) {
          const groupx = value.data.data.reduce((rv, x) => {
            // eslint-disable-next-line no-param-reassign
            (rv[x.title] = rv[x.title] || []).push(x)
            return rv
          }, {})
          const sectionList = Object.keys(groupx).map((item) => ({
            title: item,
            data: groupx[item],
          }))
          setSpecList(sectionList)
        }
        setLoadingSpec(false)
      })
      .catch((reason) => {
        console.log(reason)
        setLoadingSpec(false)
        Toast.show({
          type: 'error',
          text1: reason.message,
        })
      })
  }

  useEffect(() => {
    getProvinceList()
    getSpecList()
  }, [])

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      renderTabBar={(props) => (
        <TabBar
          {...props}
          renderLabel={({ route }) => (
            <Text style={{ ...fonts.body4, color: colors.gray }}>
              {route.title}
            </Text>
          )}
          labelStyle={{
            color: colors.gray,
          }}
          indicatorStyle={{
            backgroundColor: colors.lightPurple,
          }}
          style={{ backgroundColor: colors.white }}
        />
      )}
      initialLayout={{ width: layout.width }}
    />
  )
}

export default BienSoXe
