import React, { useState } from 'react'
import { FlatList, StyleSheet, View } from 'react-native'
import axios from 'axios'
import qs from 'qs'
import Modal from 'react-native-modal'
import { Feather } from '@expo/vector-icons'
import Dropdown from '../../components/Dropdown'
import nongDoCon from '../../constants/nongDoCon'
import { colors, sizes } from '../../theme'
import FormInput from '../../components/FormInput'
import Checkbox from '../../components/Checkbox'
import Button from '../../components/Button'
import IconLabel from '../../components/IconLabel'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: sizes.padding,
  },
})

const NongDoCon = () => {
  const [gender, setGender] = useState(1)
  const [weight, setWeight] = useState(0)
  const [time, setTime] = useState(0)
  const [ml, setMl] = useState(0)
  const [drinkType, setDrinkType] = useState(1)
  const [ethanol, setEthanol] = useState('')
  const [noneDrinkMatch, setNoneDrinkMatch] = useState(false)
  const [loading, setLoading] = useState(false)
  const [formError, setFormError] = useState({
    weight: '',
    time: '',
    ethanol: '',
    ml: '',
  })
  const [showResult, setShowResult] = useState(false)
  const [checkResult, setCheckResult] = useState(undefined)

  const canCheck = () => {
    if (
      formError.time
      || formError.weight
      || formError.ethanol
      || formError.ml
      || !ethanol
      || ml <= 0
      || time <= 0
      || weight <= 0
    ) {
      return false
    }
    return true
  }

  const onCheck = async () => {
    setLoading(true)
    const data = {
      gioi_tinh: gender,
      can_nang: weight,
      the_tich_uong: ml,
      phan_tram_ethanol: ethanol,
      thoi_gian_can_do: time,
    }
    const options = {
      method: 'POST',
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      data: qs.stringify(data),
      url: 'https://chac.vn/do-nong-do-con-online/chac-calculator.php',
    }

    axios(options)
      .then((result) => {
        setLoading(false)
        const r = result.data
        const remain = (r.c_peak * time) / (r.thoi_gian_thai_het * 60)
        console.log(remain)
        let ok = false
        if (remain <= 0.25) ok = true
        setCheckResult({ ...result.data, ok, remain })
        setShowResult(true)
      })
      .catch((err) => {
        console.log(err)
        setLoading(false)
      })
  }

  return (
    <View style={[styles.container]}>
      <Modal
        isVisible={showResult}
        onSwipeComplete={() => setShowResult(false)}
        onBackdropPress={() => setShowResult(false)}
        swipeDirection="left"
      >
        {showResult ? (
          <View
            style={{
              backgroundColor: colors.lightGray1,
              padding: sizes.padding,
              borderRadius: sizes.radius,
            }}
          >
            <IconLabel
              imageType={false}
              icon={
                <Feather name="chevrons-right" size={18} color={colors.red} />
              }
              label="Nồng độ cồn trong hơi thở cho phép: 0.25 mg/L"
            />
            <IconLabel
              imageType={false}
              icon={
                <Feather name="chevrons-right" size={18} color={colors.blue} />
              }
              label={`Sau thời gian ${time} phút: Nồng độ cồn trong cơ thể là: ${
                checkResult.remain
              } mg/L ${
                checkResult.ok
                  ? '(Chỉ số đo của bạn hoàn toàn bình thường)'
                  : '(Bạn sẽ bị phạt khi tham gia giao thông)'
              }`}
            />
            <IconLabel
              imageType={false}
              icon={
                <Feather name="chevrons-right" size={18} color={colors.blue} />
              }
              label={`Số thời gian để thải hết cồn ra khỏi cơ thể: ${
                checkResult.thoi_gian_thai_het * 60
              } phút`}
            />
          </View>
        ) : (
          <></>
        )}
      </Modal>
      <FlatList
        data={[]}
        showsVerticalScrollIndicator={false}
        bounces={false}
        ListHeaderComponent={(
          <View>
            <Dropdown
              data={nongDoCon.gender}
              label="Giới tính"
              defaultValue={nongDoCon.gender[0]}
              onSelect={setGender}
              selected={gender}
            />
            <FormInput
              label="Cân nặng (kg)"
              containerStyle={{
                marginTop: sizes.base,
              }}
              placeholder="Cân nặng"
              onChange={(value) => {
                if (value) {
                  setWeight(value)
                  setFormError((prevState) => ({ ...prevState, weight: '' }))
                } else {
                  setFormError((prevState) => ({
                    ...prevState,
                    weight: 'Bạn chưa nhập cân nặng',
                  }))
                }
              }}
              errorMsg={formError.weight}
            />
            <View style={{ marginTop: sizes.base }}>
              <Dropdown
                data={nongDoCon.drinkTypes}
                defaultValue={nongDoCon.drinkTypes[0]}
                label="Loại nước uống"
                onSelect={setDrinkType}
              />
            </View>
            {drinkType > 0 ? (
              <View style={{ marginTop: sizes.base }}>
                {drinkType === 1 ? (
                  <Dropdown
                    data={nongDoCon.alcoholTypes}
                    defaultValue={nongDoCon.alcoholTypes[0]}
                    label="Chọn rượu"
                    onSelect={setEthanol}
                  />
                ) : (
                  <Dropdown
                    data={nongDoCon.beerTypes}
                    defaultValue={nongDoCon.beerTypes[0]}
                    label="Chọn bia"
                    onSelect={setEthanol}
                  />
                )}
              </View>
            ) : null}
            <View
              style={{
                marginTop: sizes.base,
                paddingVertical: sizes.radius,
              }}
            >
              <Checkbox
                checked={noneDrinkMatch}
                label="Không có loại rượu nào phù hợp"
                onPress={(value) => setNoneDrinkMatch(value)}
              />
            </View>
            {noneDrinkMatch ? (
              <FormInput
                label="% nồng độ cồn trong thức uống"
                containerStyle={{
                  marginTop: sizes.base,
                }}
                placeholder="Tỷ lệ ethanol trong thức uống"
                onChange={(value) => {
                  if (value) {
                    setEthanol(value)
                    setFormError((prevState) => ({ ...prevState, ethanol: '' }))
                  } else if (noneDrinkMatch) {
                    setFormError((prevState) => ({
                      ...prevState,
                      ethanol: 'Bạn chưa nhập thời gian',
                    }))
                  } else {
                    setFormError((prevState) => ({ ...prevState, ethanol: '' }))
                  }
                }}
                errorMsg={formError.ethanol}
              />
            ) : null}
            <FormInput
              label="Số ml rượu đã uống"
              containerStyle={{
                marginTop: sizes.base,
              }}
              placeholder="Số ml rượu đã uống"
              onChange={(value) => {
                if (value) {
                  setMl(value)
                  setFormError((prevState) => ({ ...prevState, ml: '' }))
                } else {
                  setFormError((prevState) => ({
                    ...prevState,
                    ml: 'Bạn chưa nhập thông tin',
                  }))
                }
              }}
              errorMsg={formError.ml}
            />
            <FormInput
              label="Thời gian (phút)"
              containerStyle={{
                marginTop: sizes.base,
              }}
              placeholder="Thời gian"
              onChange={(value) => {
                if (value) {
                  setTime(value)
                  setFormError((prevState) => ({ ...prevState, time: '' }))
                } else {
                  setFormError((prevState) => ({
                    ...prevState,
                    time: 'Bạn chưa nhập thời gian',
                  }))
                }
              }}
              errorMsg={formError.time}
            />
          </View>
        )}
        renderItem={() => <></>}
      />
      <Button
        loading={loading}
        label="Kiểm tra"
        disabled={!canCheck()}
        onPress={onCheck}
      />
    </View>
  )
}

export default NongDoCon
