import React, { useEffect, useState } from 'react'
import MapView from 'react-native-maps'
import {
  AppState, FlatList, Platform, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View,
} from 'react-native'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import Constants from 'expo-constants'
import { MaterialCommunityIcons, SimpleLineIcons } from '@expo/vector-icons'
import * as Location from 'expo-location'
import axios from 'axios'
import mapData from '../../constants/mapData'
import { colors, sizes } from '../../theme'
import OptionItem from '../../components/OptionItem'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchBox: {
    marginTop: Constants.statusBarHeight,
    borderRadius: 5,
    paddingHorizontal: sizes.radius,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  chipsScrollView: {
    height: 35 + sizes.base * 2,
  },
  chipsIcon: {
    marginRight: 5,
  },
  chipsItem: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 0,
    marginTop: sizes.base,
    alignItems: 'center',
    paddingHorizontal: 20,
    marginHorizontal: sizes.radius,
    height: 35,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.25,
    shadowRadius: 3,
    elevation: 5,
  },
})

const Place = ({ navigation }) => {
  const mapRef = React.createRef()
  const [region, setRegion] = useState(mapData.region)
  const [loading, setLoading] = useState(false)
  const [location, setLocation] = useState(null)
  const [hasLocationPermissions, setHasLocationPermissions] = useState(false)
  const [selected, setSelected] = useState(1)
  const [searchKeyword, setSearchKeyword] = useState('')
  const [searchResults, setSearchResults] = useState([])
  const [isShowingResults, setIsShowingResults] = useState(false)

  const onLocationSelect = (data, details) => {
    console.log(data)
    console.log(details)
    setLocation({
      latitude: details.geometry.location.lat,
      longitude: details.geometry.location.lng,
    })
    mapRef.current.animateToCoordinate({
      latitude: location.latitude,
      longitude: location.longitude,
    })
  }

  const getCurrentLocation = async () => {
    setLoading(true)
    const { status } = await Location.requestForegroundPermissionsAsync()
    if (status !== 'granted') {
      setHasLocationPermissions(false)
      setLoading(false)
      return
    }
    setHasLocationPermissions(true)
    const loc = await Location.getCurrentPositionAsync({})
    setLocation({
      latitude: loc.coords.latitude,
      longitude: loc.coords.longitude,
    })
    setRegion((prevState) => ({
      latitude: loc.coords.latitude,
      longitude: loc.coords.longitude,
      latitudeDelta: prevState.latitudeDelta,
      longitudeDelta: prevState.longitudeDelta,
    }))
    setLoading(false)
  }

  const requestPermission = async () => {
    const { status } = await Location.requestForegroundPermissionsAsync()
    if (status !== 'granted') {
      setHasLocationPermissions(false)
    } else {
      setHasLocationPermissions(true)
    }
  }

  const handleAppStateChange = () => {
    requestPermission()
  }

  const searchLocation = async (text) => {
    setSearchKeyword(text)
    axios
      .request({
        method: 'post',
        url: `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${mapData.GOOGLE_MAP_KEY}&input=${searchKeyword}`,
      })
      .then((response) => {
        console.log(response.data)
        setSearchResults(response.data.predictions)
        setIsShowingResults(true)
      })
      .catch((e) => {
        console.log(e.response)
        setIsShowingResults(false)
        setSearchResults([])
      })
  }

  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange)

    return () => {
      AppState.removeEventListener('change', handleAppStateChange)
    }
  }, [])

  function renderScrollChipView() {
    return (
      <ScrollView
        horizontal
        scrollEventThrottle={1}
        showsHorizontalScrollIndicator={false}
        height={50}
        style={styles.chipsScrollView}
      >
        {mapData.mapCategory.map((category, index) => (
          <TouchableOpacity
            key={index}
            style={[styles.chipsItem, {
              borderWidth: selected === category.id ? 2 : 0,
              borderColor: colors.lightPurple,
            }]}
            onPress={() => setSelected(category.id)}
          >
            {category.icon}
            <Text>{category.name}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    )
  }

  function renderButton() {
    return (
      <View
        style={{
          position: 'absolute',
          bottom: sizes.padding * 3,
          right: sizes.padding,
        }}
      >
        <OptionItem
          icon={<MaterialCommunityIcons name="home" size={24} color={colors.white} />}
          iconType
          bgColor={colors.gradient3}
          onPress={() => navigation.goBack()}
        />
        {
          hasLocationPermissions ? (
            <OptionItem
              containerStyle={{
                marginTop: sizes.base,
              }}
              icon={<SimpleLineIcons name="location-pin" size={24} color={colors.white} />}
              iconType
              bgColor={colors.gradient4}
              onPress={() => (!loading ? getCurrentLocation() : {})}
              loading={loading}
            />
          ) : null
        }
      </View>
    )
  }

  // eslint-disable-next-line no-unused-vars
  function renderCustomSearch() {
    return (
      <View style={styles.searchBox}>
        <TextInput
          placeholder="Bạn muốn đi đâu?"
          returnKeyType="search"
          onChangeText={(text) => searchLocation(text)}
          value={searchKeyword}
        />
        {
           isShowingResults && (
             <FlatList
               data={searchResults}
               renderItem={({ item, index }) => (
                 <TouchableOpacity
                   key={`${index}`}
                   style={styles.resultItem}
                   onPress={() => {
                     setSearchKeyword(item.description)
                     setIsShowingResults(false)
                   }}
                 >
                   <Text>{item.description}</Text>
                 </TouchableOpacity>
               )}
               keyExtractor={(item) => item.id}
               // style={styles.searchResultsContainer}
             />
           )
         }
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <MapView
        ref={mapRef}
        region={region}
        style={{ ...StyleSheet.absoluteFillObject }}
        provider="google"
        showsUserLocation
        followsUserLocation
        showsMyLocationButton={false}
        loadingEnabled
        loadingIndicatorColor={colors.lightPurple}
        onMapReady={requestPermission}
        scrollEnabled
      />
      <View style={styles.searchBox}>
        <GooglePlacesAutocomplete
          placeholder="Bạn muốn đi đâu?"
          minLength={2}
          fetchDetails
          numberOfLines={2}
          isRowScrollable={false}
          query={{
            key: mapData.GOOGLE_MAP_KEY,
            language: 'vi',
            components: 'country:vn',
            radius: 2000,
            rankby: 'distance',
          }}
          onPress={(data, details = null) => onLocationSelect(data, details)}
          nearbyPlacesAPI="GooglePlacesSearch"
          enablePoweredByContainer={false}
          onFail={(error) => console.log(error)}
          debounce={200}
          currentLocation
          currentLocationLabel="Vị trí hiện tại"
        />
      </View>
      <View style={{
        marginTop: Constants.statusBarHeight + (Platform.OS === 'android' ? 20 : 0),
      }}
      >
        {renderScrollChipView()}
      </View>
      {renderButton()}
    </View>
  )
}

export default Place
