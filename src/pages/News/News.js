import React from 'react'
import WebView from 'react-native-webview'
import { ActivityIndicator, SafeAreaView } from 'react-native'

function LoadingIndicatorView() {
  return <ActivityIndicator color="#009b88" size="large" />
}

const News = () => (
  <SafeAreaView
    style={{
      flex: 1,
    }}
  >
    <WebView
      source={{ uri: 'https://tinxe.vn/tin-tuc' }}
      renderLoading={LoadingIndicatorView}
      startInLoadingState
      originWhitelist={['*']}
    />
  </SafeAreaView>
)

export default News
