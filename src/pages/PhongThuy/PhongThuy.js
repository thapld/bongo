import React, { useEffect, useState } from 'react'
import {
  ScrollView,
  StyleSheet, Text, TouchableOpacity, View,
} from 'react-native'
import Modal from 'react-native-modal'
import { AntDesign } from '@expo/vector-icons'
import FormInput from '../../components/FormInput'
import Dropdown from '../../components/Dropdown'
import phongThuy from '../../constants/phongThuy'
import { colors, fonts, sizes } from '../../theme'
import Button from '../../components/Button'
import phongThuyUtil from '../../utils/phongThuyUtil'
import IconLabel from '../../components/IconLabel'
import LineDivider from '../../components/LineDivider'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: sizes.padding,
  },
  formItem: {
    marginTop: sizes.base,
  },
  formButton: {
    marginTop: sizes.padding,
  },
  color: {
    width: 16,
    height: 16,
    borderRadius: 8,
  },
  colorContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: sizes.padding,
    paddingVertical: sizes.base,
  },
})

const PhongThuy = () => {
  const [bsx, setBsx] = useState('')
  const [dob, setDob] = useState(0)
  const [menh, setMenh] = useState(undefined)
  const [showHDPT, setShowHDPT] = useState(false)
  const [colorList, setColorList] = useState(undefined)
  const [visible, setVisible] = useState(false)
  const [loading] = useState(false)
  const [result, setResult] = useState(undefined)
  const [formError, setFormError] = useState({ bsx: '', dob: '', menh: '' })
  const isEnable = () => bsx && dob && menh && !formError.bsx && !formError.dob

  const onCheck = () => {
    const sothuandeu = phongThuyUtil.checkSoThuanDeu(bsx)
    const std = {
      type: '',
      dich: '',
      ok: false,
    }
    if (sothuandeu.tuquy) {
      const d = phongThuyUtil.getBienSoTuquy(bsx)
      if (d) {
        std.type = `Biển ${sothuandeu.nguquy ? 'ngũ' : 'tứ'} quý ${sothuandeu.value}`
        std.dich = d.label
        std.ok = true
      }
    }

    const soganh = phongThuyUtil.checkSoDoi2Phia(bsx) ? 'Có' : 'Không'
    const sohp = phongThuyUtil.checkSoCanBangHaiBen(bsx) ? 'Có' : 'Không'
    const sot = phongThuyUtil.checkSoTienDan(bsx, 1)
      || phongThuyUtil.checkSoTienDan(bsx, 2)
      || phongThuyUtil.checkSoTienDan(bsx, 3) ? 'Có' : 'Không'
    const b80 = phongThuyUtil.getBienSo80(bsx)
    const ptds = phongThuyUtil.getPhongThuyDaySo(bsx)

    const menhy = phongThuyUtil.getMenh(dob)
    const mm = phongThuy.menhmau.find((item) => item.menh === menhy.menh.value)
    const mauHopMenh = mm.tuong_sinh
      .concat(mm.hoa_hop)
      .map((item) => phongThuy.menhColor.find((i) => i.id === item))
    const ptbx = phongThuyUtil.getPhongThuyBsx(bsx, menh)
    const tc1 = phongThuyUtil.checkKinhDichTc1(bsx)
    const tc2 = phongThuyUtil.checkKinhDichTc2(bsx)
    const tc3 = phongThuyUtil.checkKinhDichTc3(bsx)
    setResult({
      menh: menhy,
      mauHopMenh,
      std,
      soganh,
      sohp,
      sot,
      b80,
      ptds,
      ptbx,
      tc1,
      tc2,
      tc3,
    })
    console.log(result)
    setVisible(true)
  }

  useEffect(() => {
    setColorList(
      phongThuy.menhmau.map((item) => ({
        value: item.menh,
        label: item.tuong_sinh
          .concat(item.hoa_hop)
          .map((x) => phongThuy.menhColor.find((i) => i.id === x).name)
          .join(','),
      })),
    )
  }, [])

  function renderResult() {
    return (
      <View style={{
        backgroundColor: colors.lightGray1,
        padding: sizes.padding,
        borderRadius: sizes.radius,
        marginTop: sizes.padding,
      }}
      >
        <IconLabel
          labelStyle={{
            ...fonts.body4,
          }}
          containerStyle={{
            paddingVertical: sizes.size_2,
            paddingHorizontal: sizes.size_2,
          }}
          label={`➤ Tuổi: ${result.menh.can.label} ${result.menh.chi.label}`}
        />
        <IconLabel
          labelStyle={{
            ...fonts.body4,
          }}
          containerStyle={{
            paddingVertical: sizes.size_2,
            paddingHorizontal: sizes.size_2,
          }}
          label={`➤ Mệnh: ${result.menh.menh.label}`}
        />
        <IconLabel
          labelStyle={{
            ...fonts.body4,
          }}
          containerStyle={{
            paddingVertical: sizes.size_2,
            paddingHorizontal: sizes.size_2,
          }}
          label={`➤ Màu hợp: ${result.mauHopMenh.map((item) => item.name).join(',')}`}
        />
        <View style={styles.colorContainer}>
          {result.mauHopMenh
            .map((item) => item.color)
            .map((item, index) => (
              <View
                key={`color-${index}`}
                style={[styles.color, { backgroundColor: item }]}
              />
            ))}
        </View>
        <LineDivider
          lineStyle={{
            backgroundColor: colors.grayx,
            marginVertical: sizes.radius,
          }}
        />
        <View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: sizes.base,
            }}
          >
            <Text>Theo phong thủy</Text>
            <TouchableOpacity
              style={{
                marginLeft: sizes.base,
              }}
              onPress={() => setShowHDPT(true)}
            >
              <AntDesign
                name="questioncircleo"
                size={15}
                color={colors.gray}
              />
            </TouchableOpacity>
          </View>
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label="➤ Biển số ấn tượng:"
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`  ☞ Thuần đều (tứ quý, ngũ quý): ${result.std.ok ? (`${result.std.type} - ${result.std.dich}`) : 'Không'}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`  ☞ Biển số gánh (số đối hai bên): ${result.soganh}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`  ☞ Biển số hạnh phúc (số cân bằng hai bên): ${result.sohp}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`  ☞ Số tiến: ${result.sot}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`  ☞ Biển số 80: ${result.b80}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`  ☞ Phong thuỷ Trung Quốc: ${result.ptds.trungquoc ?? 'Không'}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`  ☞ Phong thuỷ dãy số: ${result.ptds.dayso ?? 'Không'}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`➤ Biển số xe hợp xe: ${result.ptbx.label}`}
          />
        </View>
        <LineDivider
          lineStyle={{
            backgroundColor: colors.grayx,
            marginVertical: sizes.radius,
          }}
        />
        <View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: sizes.base,
            }}
          >
            <Text>Theo kinh dịch</Text>
          </View>
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`➤ Tiêu chí 1: Quẻ ${result.tc1.name} - ${result.tc1.type} - ${result.tc1.dich}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`➤ Tiêu chí 2: Quẻ ${result.tc2.name} - ${result.tc2.type} - ${result.tc2.dich}`}
          />
          <IconLabel
            labelStyle={{
              ...fonts.body4,
            }}
            containerStyle={{
              paddingVertical: sizes.size_2,
              paddingHorizontal: sizes.size_2,
            }}
            label={`➤ Tiêu chí 3: Quẻ ${result.tc3.name} - ${result.tc3.type} - ${result.tc3.dich}`}
          />
        </View>
      </View>
    )
  }

  if (!colorList) return null

  return (
    <ScrollView>
      <View style={styles.container}>
        <Modal
          isVisible={showHDPT}
          onSwipeComplete={() => setShowHDPT(false)}
          onBackdropPress={() => setShowHDPT(false)}
          swipeDirection="left"
        >
          <View
            style={{
              backgroundColor: colors.lightGray1,
              padding: sizes.padding,
              borderRadius: sizes.radius,
            }}
          >
            <Text>
              - Số xe có hành sinh phù cho hành của màu xe (dể sinh phù cho người
              chủ xe. Ở đây xe không sinh phù trực tiếp cho người, vì số xe chỉ là
              một bộ phận của xe, vì xe còn nhiều bộ phận khác nữa như số máy, số
              khung...
            </Text>
            <Text>
              - Trong hai yếu tố cấu thành số xe đệp thì yếu tố ấn tượng quan
              trọng hơn, vì yếu tố hợp với xe chỉ hỗ trợ thêm cho xe mà thôi.
            </Text>
          </View>
        </Modal>
        <FormInput
          label="Năm sinh"
          keyboardType="numeric"
          onChange={(text) => {
            const reg = /^[0-9]{4}$/
            if (text && reg.test(text)) {
              setDob(text)
              setFormError((prevState) => (
                {
                  ...prevState,
                  dob: '',
                }
              ))
            } else {
              setFormError((prevState) => (
                {
                  ...prevState,
                  dob: 'Ngày sinh không hợp lệ',
                }
              ))
            }
          }}
          errorMsg={formError.dob}
        />
        <FormInput
          label="Biển số xe (4-5 số)"
          keyboardType="numeric"
          containerStyle={styles.formItem}
          onChange={(text) => {
            const reg = /^[0-9]{4,5}$/
            if (text && reg.test(text)) {
              setBsx(text)
              setFormError((prevState) => (
                {
                  ...prevState,
                  bsx: '',
                }
              ))
            } else {
              setFormError((prevState) => (
                {
                  ...prevState,
                  bsx: 'Biển số xe không hợp lệ',
                }
              ))
            }
          }}
          errorMsg={formError.bsx}
        />
        <Dropdown
          label="Màu xe"
          containerStyle={styles.formItem}
          panelStyle={{
            marginHorizontal: sizes.padding,
          }}
          data={colorList}
          defaultValue={colorList[0]}
          onSelect={(value) => setMenh(value)}
        />
        <Button
          label="Kiểm tra"
          disabled={!isEnable()}
          loading={loading}
          containerStyle={styles.formButton}
          onPress={() => onCheck()}
        />
        {visible ? (
          renderResult()
        ) : (
          <></>
        )}
      </View>
    </ScrollView>
  )
}

export default PhongThuy
