import React from 'react'
import PropTypes from 'prop-types'
import {
  FlatList, SafeAreaView, StatusBar, View,
} from 'react-native'
import { AntDesign, Feather, Octicons } from '@expo/vector-icons'
import dummyData from '../../constants/dummyData'
import FlatListSlider from '../../components/Slider/FlatListSlider'
import Preview from '../../components/Slider/Preview'
import { colors, images, sizes } from '../../theme'
import OptionItem from '../../components/OptionItem'
import Section from '../../components/Section/Section'
import SignalVerticalCard from '../../components/SignalVerticalCard/SignalVerticalCard'
import QuizBox from '../../components/QuizBox/QuizBox'

const Home = ({ navigation }) => {
  function renderNews() {
    return (
      <Section
        title="Tin tức bổi bật"
        headerContainerStyle={{
          marginTop: 20,
          marginBottom: 0,
        }}
        actionLabel="TINXE.VN"
      >
        <FlatListSlider
          data={dummyData.newsData}
          width={275}
          timer={4000}
          component={<Preview />}
          onPress={(item) => console.log(JSON.stringify(item))}
          indicatorActiveWidth={20}
          indicatorActiveColor={colors.lightPurple}
          indicatorContainerStyle={{
            marginTop: 0,
          }}
          contentContainerStyle={{
            paddingHorizontal: 16,
          }}
        />
      </Section>
    )
  }

  function renderBoard() {
    return (
      <Section hasTitle={false} hasAction={false}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
          }}
        >
          <View
            style={{ flexDirection: 'row', paddingHorizontal: sizes.padding }}
          >
            <OptionItem
              iconType
              icon={<Feather name="book-open" size={28} color={colors.white} />}
              bgColor={colors.jshine}
              label="Thi GPLX"
              onPress={() => console.log('GPLX clicked')}
            />
            <OptionItem
              iconType
              icon={<Octicons name="law" size={28} color={colors.white} />}
              bgColor={colors.gradient2}
              label="Luật lệ"
              onPress={() => console.log('Luật giao thông')}
            />
            <OptionItem
              iconType
              icon={<Feather name="map-pin" size={28} color={colors.white} />}
              bgColor={colors.gradient3}
              label="Địa điểm"
              onPress={() => navigation.navigate('Place')}
            />
            <OptionItem
              iconType
              icon={<Feather name="x-octagon" size={28} color={colors.white} />}
              bgColor={colors.gradient4}
              label="Phạt nguội"
              onPress={() => navigation.navigate('PhatNguoi')}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: sizes.padding,
              paddingHorizontal: sizes.padding,
            }}
          >
            <OptionItem
              iconType={false}
              source={images.driver_license}
              bgColor={colors.gradient8}
              label="GPLX"
              onPress={() => console.log('Địa điểm')}
            />
            <OptionItem
              iconType
              icon={
                <AntDesign name="dashboard" size={28} color={colors.white} />
              }
              bgColor={colors.gradient5}
              label="Nồng độ cồn"
              onPress={() => navigation.navigate('NongDoCon')}
            />
            <OptionItem
              iconType={false}
              source={images.license_plate}
              bgColor={colors.gradient6}
              label="Biển số xe"
              onPress={() => navigation.navigate('BienSoXe')}
            />
            <OptionItem
              iconType={false}
              source={images.star}
              bgColor={colors.gradient7}
              label="Phong thủy"
              onPress={() => navigation.navigate('PhongThuy')}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: sizes.padding,
              paddingHorizontal: sizes.padding,
            }}
          />
        </View>
      </Section>
    )
  }

  function renderSignal() {
    return (
      <Section title="Biển báo ngẫu nhiên" actionLabel="Tất cả">
        <FlatList
          keyExtractor={(item) => `${item.id}`}
          data={dummyData.signals}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => (
            <SignalVerticalCard
              containerStyle={{
                marginVertical: 5,
                marginLeft: index === 0 ? sizes.padding : 18,
                marginRight:
                  index === dummyData.signals.length - 1 ? sizes.padding : 0,
              }}
              key={`ps-${index}`}
              item={item}
              onPress={() => console.log('Vertical Food Card')}
            />
          )}
        />
      </Section>
    )
  }

  function renderQuiz() {
    return (
      <Section title="Câu hỏi ngẫu nhiên" actionLabel="Tất cả">
        <QuizBox guide="Chọn một trong những đáp án phía dưới để trắc nghiệm nhanh, ôn lại kiến thức" />
      </Section>
    )
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar barStyle="dark-content" />
      <FlatList
        keyExtractor={(item) => item.id}
        data={dummyData.menu}
        bounces={false}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={(
          <View>
            {renderNews()}
            {renderBoard()}
            {renderSignal()}
            {renderQuiz()}
          </View>
        )}
        renderItem={() => <></>}
      />
    </SafeAreaView>
  )
}

Home.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}

Home.defaultProps = {
  navigation: { navigate: () => null },
}

export default Home
