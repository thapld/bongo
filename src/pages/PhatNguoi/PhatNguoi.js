import React, { useState } from 'react'
import {
  FlatList, Text, TouchableOpacity, View,
} from 'react-native'
import axios from 'axios'
import Toast from 'react-native-toast-message'
import FormInput from '../../components/FormInput'
import { colors, globalStyles, sizes } from '../../theme'
import RadioButton from '../../components/RadioButton'
import DualText from '../../components/DualText/DualText'
import LineDivider from '../../components/LineDivider'
import Button from '../../components/Button'
import api from '../../constants/api'

const PhatNguoi = () => {
  const [licensePlate, setLicensePlate] = useState('')
  const [plateError, setPlateError] = useState('')
  const [vehicleType, setVehicleType] = useState(1)
  const [loading, setLoading] = useState(false)
  const [violation, setViolation] = useState({
    list: [],
    statistic: { penalize: 0, notPenalize: 0 },
  })

  function isEnableCheck() {
    return licensePlate !== '' && plateError === ''
  }

  const onCheck = () => {
    setLoading(true)
    axios
      .get(
        `${
          api.baseUrl
        }/bsx/phat-nguoi?licensePlate=${licensePlate.trim()}&type=${vehicleType}`,
      )
      .then((value) => {
        if (value && value.data && value.data.data) {
          const list = value.data.data
          setViolation({
            list,
            statistic: {
              penalize: list
                .filter(
                  (item) => item.status.toUpperCase().trim() === 'ĐÃ XỬ PHẠT',
                )
                .count(),
              notPenalize: list
                .filter(
                  (item) => item.status.toUpperCase().trim() === 'CHƯA XỬ PHẠT',
                )
                .count(),
            },
          })
        }
        setLoading(false)
      })
      .catch((reason) => {
        setLoading(false)
        Toast.show({
          type: 'error',
          text1: reason.message,
        })
      })
  }

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
      }}
    >
      <Toast />
      <View
        style={{
          flex: 1,
          paddingHorizontal: sizes.padding,
        }}
      >
        <FlatList
          keyExtractor={(item) => item.id}
          data={violation.list}
          bounces={false}
          showsVerticalScrollIndicator={false}
          alwaysBounceVertical
          ItemSeparatorComponent={() => (
            <LineDivider lineStyle={{ marginTop: sizes.radius }} />
          )}
          ListHeaderComponent={(
            <View
              style={{
                flex: 1,
              }}
            >
              <Text style={{ ...globalStyles.labelStyle }}>
                Loại phương tiện
              </Text>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: sizes.base,
                  justifyContent: 'space-between',
                }}
              >
                <RadioButton
                  label="Ô tô"
                  isSelected={vehicleType === 1}
                  onPress={() => setVehicleType(1)}
                />
                <RadioButton
                  label="Xe máy"
                  isSelected={vehicleType === 2}
                  onPress={() => setVehicleType(2)}
                />
                <RadioButton
                  label="Xe máy điện"
                  isSelected={vehicleType === 3}
                  onPress={() => setVehicleType(3)}
                />
              </View>
              <FormInput
                label="Thông tin xe"
                placeholder="Nhập biển số xe"
                onChange={(value) => {
                  if (value) {
                    setLicensePlate(value)
                    setPlateError('')
                    setViolation({
                      list: [],
                      statistic: {
                        penalize: 0,
                        notPenalize: 0,
                      },
                    })
                  } else {
                    setPlateError('Bạn chưa nhập biển số')
                    setViolation({
                      list: [],
                      statistic: {
                        penalize: 0,
                        notPenalize: 0,
                      },
                    })
                  }
                }}
                errorMsg={plateError}
                containerStyle={{
                  marginTop: sizes.base,
                }}
              />
              <Button
                label="Kiểm tra"
                containerStyle={{
                  height: 55,
                  marginTop: sizes.radius,
                }}
                loading={loading}
                disabled={!isEnableCheck()}
                onPress={() => onCheck()}
              />
              {violation.statistic.penalize + violation.statistic.notPenalize
                > 0 && (
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: sizes.padding,
                  }}
                >
                  <TouchableOpacity
                    style={{
                      borderRadius: sizes.radius,
                      backgroundColor: colors.green,
                      paddingHorizontal: sizes.radius,
                      paddingVertical: sizes.radius,
                    }}
                  >
                    <Text
                      style={{
                        color: colors.white,
                      }}
                    >
                      {`Đã xử phạt: ${violation.statistic.penalize}`}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      borderRadius: sizes.radius,
                      backgroundColor: colors.red,
                      paddingHorizontal: sizes.radius,
                      paddingVertical: sizes.radius,
                      marginLeft: sizes.padding,
                    }}
                  >
                    <Text
                      style={{
                        color: colors.white,
                      }}
                    >
                      {`Chưa xử phạt: ${violation.statistic.notPenalize}`}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          )}
          renderItem={({ item, index }) => (
            <View
              key={`pn-${index}`}
              style={{
                flex: 1,
                backgroundColor: colors.transparent,
                marginTop: sizes.padding,
                marginBottom:
                  index === violation.list.length - 1 ? sizes.padding * 3 : 0,
              }}
            >
              <DualText
                text1="Biển số"
                text2={item.licensePlate}
                containerStyle={{
                  marginBottom: sizes.base,
                  backgroundColor: colors.white,
                }}
              />
              <DualText
                text1="Màu biển"
                text2={item.licensePlateColor}
                containerStyle={{
                  marginBottom: sizes.base,
                  backgroundColor: colors.white,
                }}
              />
              <DualText
                text1="Phương tiện"
                text2={item.vehicleType}
                containerStyle={{
                  marginBottom: sizes.base,
                  backgroundColor: colors.white,
                }}
              />
              <DualText
                text1="Thời gian"
                text2={item.violationTime}
                containerStyle={{
                  marginBottom: sizes.base,
                  backgroundColor: colors.white,
                }}
              />
              <DualText
                text1="Địa điểm"
                text2={item.violationLocation}
                containerStyle={{
                  marginBottom: sizes.base,
                  backgroundColor: colors.white,
                }}
              />
              <DualText
                text1="Trạng thái"
                text2={item.status}
                containerStyle={{
                  marginBottom: sizes.base,
                  backgroundColor: colors.white,
                }}
              />
              <DualText
                text1="Đợn vị"
                text2={item.provider}
                containerStyle={{
                  marginBottom: sizes.base,
                  backgroundColor: colors.white,
                }}
              />
              <DualText
                text1="Liên hệ"
                text2={item.contractMobile}
                containerStyle={{
                  marginBottom: sizes.base,
                  backgroundColor: colors.white,
                }}
              />
            </View>
          )}
          style={{
            paddingVertical: sizes.radius,
          }}
        />
      </View>
    </View>
  )
}

export default PhatNguoi
