import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, TouchableOpacity } from 'react-native'
import FontIcon from 'react-native-vector-icons/FontAwesome5'
import { sizes } from '../../theme'

const styles = StyleSheet.create({
  button: {
    paddingLeft: sizes.base,
  },
})

const PNHeaderLeft = ({ navigation }) => (
  <TouchableOpacity
    onPress={() => {
      navigation.goBack()
    }}
  >
    <FontIcon
      name="arrow-left"
      color="white"
      size={24}
      backgroundColor="transparent"
      style={styles.button}
    />
  </TouchableOpacity>
)

PNHeaderLeft.propTypes = {
  navigation: PropTypes.shape({
    openDrawer: PropTypes.func,
  }),
}

PNHeaderLeft.defaultProps = {
  navigation: { openDrawer: () => null },
}

export default PNHeaderLeft
