import React from 'react'
import { View } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'
import { colors, sizes } from '../../theme'

const PNHeaderRight = () => (
  <View
    style={{
      paddingRight: sizes.base,
      backgroundColor: 'transparent',
    }}
  >
    <FontAwesome5 name="history" size={24} color={colors.lightGrayPurple} />
  </View>
)

PNHeaderRight.propTypes = {}
PNHeaderRight.defaultProps = {}

export default PNHeaderRight
