function isValidEmail(value) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(value).toLowerCase())
}

function validateEmail(value, setEmailError) {
  if (value === '') {
    setEmailError('')
  } else if (isValidEmail(value)) {
    setEmailError('')
  } else {
    setEmailError('Invalid Email')
  }
}

function validatePassword(value, setPasswordError) {
  if (value.length < 9) {
    setPasswordError('Password must be 9 characters')
  } else {
    setPasswordError('')
  }
}

function validateInput(value, minLength, setError) {
  if (value.length < minLength) {
    setError('Invalid Input')
  } else {
    setError('')
  }
}

function calculateAngle(coordinates) {
  const startLat = coordinates[0].latitude
  const startLng = coordinates[0].longitude
  const endLat = coordinates[1].latitude
  const endLng = coordinates[1].longitude
  const dx = endLat - startLat
  const dy = endLng - startLng

  return (Math.atan2(dy, dx) * 180) / Math.PI
}

function removeAccents(str) {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/đ/g, 'd')
    .replace(/Đ/g, 'D')
}

const utils = {
  isValidEmail,
  validateEmail,
  removeAccents,
  validatePassword,
  validateInput,
  calculateAngle,
}

export default utils
