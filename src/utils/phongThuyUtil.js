import phongThuy from '../constants/phongThuy'

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index
}

// So tien dan,
// So can bang 2 ben (so hanh phuc)
// So doi 2 phia, so ganh
// So thuan deu, so tu quy, ngu quy
function checkSoThuanDeu(n) {
  const bsx = n.toString()
  const b4 = bsx.length > 4 ? bsx.substr(1, 4) : bsx
  const b5 = bsx.length > 4 ? bsx : null
  const chars = [...b4]
  const na = chars.filter(onlyUnique)
  let nguquy = false
  if (b5) {
    const chars5 = [...b5]
    const nb = chars5.filter(onlyUnique)
    nguquy = nb.length === 1
  }
  return {
    tuquy: na.length === 1,
    nguquy,
    value: chars[chars.length - 1],
  }
}

function checkSoCanBangHaiBen(n) {
  const chars = [...n.toString()]
  const d = Math.floor(chars.length / 2)
  for (let i = 0; i < d; i++) {
    if (chars[i] !== chars[chars.length - d + i]) {
      return false
    }
  }
  return true
}

function checkSoDoi2Phia(n) {
  const chars = [...n.toString()]
  const d = Math.floor(chars.length / 2)
  for (let i = 0; i < d; i++) {
    if (chars[i] !== chars[chars.length - i - 1]) {
      return false
    }
  }
  return true
}

function checkSoTienDan(n, step) {
  const chars = [...n.toString()]
  const first = Number(chars[0])
  for (let i = 1; i < chars.length; i++) {
    if (Number(chars[i]) !== first + i * step) {
      return false
    }
  }
  return true
}

function checkSoLuiDan(n, step) {
  const chars = [...n.toString()]
  const first = Number(chars[0])
  for (let i = 1; i < chars.length; i++) {
    if (Number(chars[i]) !== first - i * step) {
      return false
    }
  }
  return true
}

function getBienSo80(n) {
  const d = n / 80
  const md = Math.floor(d)
  const m = md - d
  let x = 80
  if (m > 0) x = m * 80
  return phongThuy.phongThuy80.find((item) => item.value === x.toString()).label
}

const getMenh = (year) => {
  const ncan = Number(year.substr(year.length - 1))
  const nchi = Number(year.substr(year.length - 2)) % 12
  const menhCan = phongThuy.menhcan.find((item) => item.label.some((i) => i === ncan)).value
  const menhChi = phongThuy.menhchi.find((item) => item.label.some((i) => i === nchi)).value
  const menhSum = menhCan + menhChi
  return {
    menh: phongThuy.menh.find((item) => (item.value === menhSum > 5 ? menhSum - 5 : menhSum)),
    can: phongThuy.can.find((item) => item.value === ncan),
    chi: phongThuy.chi.find((item) => item.value === nchi),
  }
}

const getPhongThuyBsx = (lp, cmenh) => {
  const nlps = [...lp.toString()]
  const sum = nlps
    .map((item) => Number(item))
    .reduce((partialSum, a) => partialSum + a, 0)
  let mod9 = sum % 9
  if (mod9 === 0) mod9 = 9
  const menhXe = phongThuy.nguhanh.find((item) => item.value === mod9).menh
  return phongThuy.hanhbsx.find(
    (item) => phongThuy.tuongquanhanhxe[`${menhXe}${cmenh}`] === item.value,
  )
}

const getPhongThuyDaySo = (lp) => {
  const str = lp.toString()
  const btq = phongThuy.trungquoc.find((item) => str.endsWith(item.value))
  const bds = phongThuy.dayso.find((item) => str.endsWith(item.value))
  return {
    trungquoc: btq,
    dayso: bds,
  }
}

function getBienSoTuquy(lp) {
  const chars = [...lp.toString()]
  return phongThuy.tuquy.find(
    (item) => String(item) === chars[chars.length - 1],
  )
}

function getUpDownBsx(bsx) {
  const str = bsx.toString()
  return {
    down: Number(str.substr(str.length - 2)),
    up: Number(str.substr(0, str.length - 2)),
  }
}

function getQue(up, down) {
  const mtq = up % 8
  const tq = mtq === 0 ? 8 : mtq
  const mhq = down % 8
  const hq = mhq === 0 ? 8 : mhq
  return phongThuy.que64.find((item) => item.ud[0] === tq && item.ud[1] === hq)
}

function checkKinhDichTc1(bsx) {
  const udBsx = getUpDownBsx(bsx)
  return getQue(udBsx.up, udBsx.down)
}

function checkKinhDichTc2(bsx) {
  const chars = [...bsx.toString()]
  const sum = chars
    .map((item) => Number(item))
    .reduce((partialSum, a) => partialSum + a, 0)
  const mh = sum % 6
  const h = mh === 0 ? 6 : mh
  const que1 = checkKinhDichTc1(bsx)
  const quai1 = phongThuy.quai.find((item) => item.value === que1.ud[1])
  const quai0 = phongThuy.quai.find((item) => item.value === que1.ud[0])
  const q = `${quai1.que}${quai0.que}`
  const qc = [...q]
  for (let i = 0; i < qc.length; i++) {
    if (Number(h) === Number(i + 1)) {
      qc[i] = qc[i] === '1' ? 0 : 1
    }
  }
  const nd = phongThuy.quai.find((item) => item.que === `${qc[0]}${qc[1]}${qc[2]}`).value
  const nu = phongThuy.quai.find((item) => item.que === `${qc[3]}${qc[4]}${qc[5]}`).value
  return phongThuy.que64.find((item) => item.ud[0] === nu && item.ud[1] === nd)
}

function checkKinhDichTc3(bsx) {
  const udBsx = getUpDownBsx(bsx)
  const chars = [...udBsx.down.toString()]
  return getQue(Number(chars[0]), Number(chars[1]))
}

export default {
  checkSoThuanDeu,
  checkSoTienDan,
  checkSoLuiDan,
  checkSoCanBangHaiBen,
  checkSoDoi2Phia,
  getBienSo80,
  getMenh,
  getPhongThuyBsx,
  getPhongThuyDaySo,
  getBienSoTuquy,
  checkKinhDichTc1,
  checkKinhDichTc2,
  checkKinhDichTc3,
}
