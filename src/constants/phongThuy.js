// xe-mauxe
const hanhbsx = [
  {
    value: 1,
    label: 'Số của xe sinh cho xe - rất tốt',
    status: 1,
  },
  {
    value: 2,
    label: 'Số của xe và xe đồng hành - tốt',
    status: 1,
  },
  {
    value: 3,
    label: 'Xe khắc số của xe - được',
    status: 1,
  },
  {
    value: 4,
    label: 'Số xe khắc xe hoặc xe sinh số của xe - xấu',
    status: 0,
  },
]
const tuongquanhanhxe = {
  11: 2, // thủy thủy
  14: 3, // thủy thổ
  15: 1, // thủy mộc
  12: 4, // thủy kim
  13: 4, // thủy hỏa
  41: 4,
  44: 2,
  45: 3,
  42: 1,
  43: 4,
  51: 4,
  54: 4,
  55: 2,
  52: 3,
  53: 1,
  21: 1,
  24: 4,
  25: 4,
  22: 2,
  23: 3,
  31: 3,
  34: 1,
  35: 4,
  32: 4,
  33: 2,
}

// Mệnh = MenhCan + MenhChi
const menh = [
  {
    value: 1,
    label: 'Kim',
    ngu_hanh: 2,
  },
  {
    value: 2,
    label: 'Thủy',
    ngu_hanh: 1,
  },
  {
    value: 3,
    label: 'Hỏa',
    ngu_hanh: 3,
  },
  {
    value: 4,
    label: 'Thổ',
    ngu_hanh: 4,
  },
  {
    value: 5,
    label: 'Mộc',
    ngu_hanh: 5,
  },
]

const nguhanhsinh = [1, 2, 5, 3, 4]
const nguhanhkhac = [1, 5, 4, 2, 3]

const nguhanh = [
  {
    value: 1,
    label: 'Khảm',
    huong: 'Bắc',
    menh: 2,
  },
  {
    value: 2,
    label: 'Khôn',
    huong: 'Tây Nam',
    menh: 4,
  },
  {
    value: 3,
    label: 'Chấn',
    huong: 'Đông',
    menh: 5,
  },
  {
    value: 4,
    label: 'Tốn',
    huong: 'Đông Nam',
    menh: 5,
  },
  {
    value: 5,
    label: 'Trung Cung',
    huong: 'Trung Tâm',
    menh: 4,
  },
  {
    value: 6,
    label: 'Càn',
    huong: 'Tây Bắc',
    menh: 1,
  },
  {
    value: 7,
    label: 'Đoài',
    huong: 'Tây',
    menh: 1,
  },
  {
    value: 8,
    label: 'Cấn',
    huong: 'Đông Bắc',
    menh: 4,
  },
  {
    value: 9,
    label: 'Ly',
    huong: 'Nam',
    menh: 3,
  },
]

const tong = [
  {
    value: 0,
    phien_am: 'Bất',
    hanh: 2,
    dich: 'Tượng trưng cho sự khởi đầu, sẵn sàng vương dậy bứt phá',
  },
  {
    value: 1,
    phien_am: 'Nhất',
    hanh: 2,
    dich: 'Chắn chắn, đứng đầu, độc nhất vô nhị',
  },
  {
    value: 2,
    phien_am: 'Mãi',
    hanh: 4,
    dich: 'Mãi mãi, bền lâu',
  },
  {
    value: 3,
    phien_am: 'Tài',
    hanh: 5,
    dich: 'Phát tài, nhiều tiền tài',
  },
  {
    value: 4,
    phien_am: 'Tử',
    hanh: 5,
    dich: 'Nhiều người cho rằng số 4 không đẹp vì tử mang ý nghĩa là chết',
  },
  {
    value: 5,
    phien_am: 'Ngũ',
    hanh: 4,
    dich: 'Ngũ hành, ngũ cung, những điều bí ẩn',
  },
  {
    value: 6,
    phien_am: 'Lộc',
    hanh: 1,
    dich: 'Có nhiều lộc, phúc',
  },
  {
    value: 7,
    phien_am: 'Thất',
    hanh: 1,
    dich: 'Thường được hiểu là mất',
  },
  {
    value: 8,
    phien_am: 'Phát',
    hanh: 4,
    dich: 'Phát tài, phát triển',
  },
  {
    value: 9,
    phien_am: 'Thừa',
    hanh: 3,
    dich: 'Vĩnh cửu, trường tồn',
  },
]

const phongThuy80 = [
  {
    value: '01',
    label: 'Thiện địa thái bình',
  },
  {
    value: '02',
    label: 'Không phân định',
  },
  {
    value: '03',
    label: 'Tiến tới như ý',
  },
  {
    value: '04',
    label: 'Bị bệnh',
  },
  {
    value: '05',
    label: 'Trường thọ',
  },
  {
    value: '06',
    label: 'Sống an nhàn dư dã',
  },
  {
    value: '07',
    label: 'Cương nghị quyết đoán',
  },
  {
    value: '08',
    label: 'Ý chí kiên cường',
  },
  {
    value: '09',
    label: 'Hưng tân cúc khai',
  },
  {
    value: '10',
    label: 'Vạn sự kết cục',
  },
  {
    value: '11',
    label: 'Gia vận được tốt',
  },
  {
    value: '12',
    label: 'Ý chí yếu mềm',
  },
  {
    value: '13',
    label: 'Tài chí hơn người',
  },
  {
    value: '14',
    label: 'Nước mắt thiên ngạn',
  },
  {
    value: '15',
    label: 'Đạt được phúc thọ',
  },
  {
    value: '16',
    label: 'Quý nhân hỗ trợ',
  },
  {
    value: '17',
    label: 'Vượt qua mọi khó khăn',
  },
  {
    value: '18',
    label: 'Có chí thì nên',
  },
  {
    value: '19',
    label: 'Đoàn tụ ông bà',
  },
  {
    value: '20',
    label: 'Sự nghiệp thất bại',
  },
  {
    value: '21',
    label: 'Thời vận lên xuống, thăng trầm',
  },
  {
    value: '22',
    label: 'Tiền vào như nước, tiền ra như giọt sương mai',
  },
  {
    value: '23',
    label: 'Mặt trời mọc',
  },
  {
    value: '24',
    label: 'Tài lộc đầy nhà',
  },
  {
    value: '25',
    label: 'Thông minh, nhạy bén',
  },
  {
    value: '26',
    label: 'Biến hóa kỳ dị',
  },
  {
    value: '27',
    label: 'Dục vọng vô tận',
  },
  {
    value: '28',
    label: 'Tâm lý bất an',
  },
  {
    value: '29',
    label: 'Dục vọng lớn nhưng khó thành',
  },
  {
    value: '30',
    label: 'Chết đi sống lại',
  },
  {
    value: '31',
    label: 'Tài dũng được chí',
  },
  {
    value: '32',
    label: 'Cầu được uớc thấy',
  },
  {
    value: '33',
    label: 'Gia môn hưng thịnh',
  },
  {
    value: '34',
    label: 'Gia đình tan vỡ',
  },
  {
    value: '35',
    label: 'Bình an ôn hòa',
  },
  {
    value: '36',
    label: 'Phong ba không ngừng',
  },
  {
    value: '37',
    label: 'Hiển đạt uy quyền',
  },
  {
    value: '38',
    label: 'Ý chí mềm yếu, thụ động',
  },
  {
    value: '39',
    label: 'Vinh hoa phú quý',
  },
  {
    value: '40',
    label: 'Cẩn thận được an',
  },
  {
    value: '41',
    label: 'Đức vọng cao thượng',
  },
  {
    value: '42',
    label: 'Sự nghiệp bất thành',
  },
  {
    value: '43',
    label: 'Hoa trong mưa đêm',
  },
  {
    value: '44',
    label: 'Buồn tủi, khổ đau',
  },
  {
    value: '45',
    label: 'Vận tốt',
  },
  {
    value: '46',
    label: 'Gặp nhiều chuyển biến',
  },
  {
    value: '47',
    label: 'Khai hoa nở nhụy',
  },
  {
    value: '48',
    label: 'Lập chí',
  },
  {
    value: '49',
    label: 'Nhiều điều xấu',
  },
  {
    value: '50',
    label: 'Một thành một bại',
  },
  {
    value: '51',
    label: 'Thịnh yếu xen kẽ',
  },
  {
    value: '52',
    label: 'Biết trước được việc',
  },
  {
    value: '53',
    label: 'Nội tâm ưu sầu',
  },
  {
    value: '54',
    label: 'May rủi song hành',
  },
  {
    value: '55',
    label: 'Ngoài tốt trong khổ',
  },
  {
    value: '56',
    label: 'Thảm thương',
  },
  {
    value: '57',
    label: 'Cây thông trong vườn tuyết',
  },
  {
    value: '58',
    label: 'Khổ trước sướng sau',
  },
  {
    value: '59',
    label: 'Mất phương hướng',
  },
  {
    value: '60',
    label: 'Tối tăm không ánh sáng',
  },
  {
    value: '61',
    label: 'Danh lợi đầy đủ',
  },
  {
    value: '62',
    label: 'Căn bản yếu kém',
  },
  {
    value: '63',
    label: 'Đạt được vinh hoa phú quý',
  },
  {
    value: '64',
    label: 'Cốt nhục chia cắt',
  },
  {
    value: '65',
    label: 'Phú quý trường thọ',
  },
  {
    value: '66',
    label: 'Trong ngoài không hòa nhã',
  },
  {
    value: '67',
    label: 'Đường danh lợi thông suốt',
  },
  {
    value: '68',
    label: 'Lập nghiệp thương gia',
  },
  {
    value: '69',
    label: 'Đứng ngồi không yên',
  },
  {
    value: '70',
    label: 'Diệt vong thế hệ',
  },
  {
    value: '71',
    label: 'Tinh thần khó chịu',
  },
  {
    value: '72',
    label: 'Suối vàng chờ đợi',
  },
  {
    value: '73',
    label: 'Ý chí cao mà sức yếu',
  },
  {
    value: '74',
    label: 'Hoàn cảnh gặp bất trắc',
  },
  {
    value: '75',
    label: 'Thủ được binh an',
  },
  {
    value: '76',
    label: 'Vĩnh biệt ngàn thu',
  },
  {
    value: '77',
    label: 'Vui sướng cực đỉnh',
  },
  {
    value: '78',
    label: 'Gia đình buồn tủi',
  },
  {
    value: '79',
    label: 'Hồi sức',
  },
  {
    value: '80',
    label: 'Gặp nhiều xui xẻo',
  },
]

const dayso = [
  {
    value: '04004',
    label: 'Không chết, không không chết (bất tử)',
  },
  {
    value: '3777',
    label: 'Ông trời',
  },
  {
    value: '3878',
    label:
      'Ông địa nhỏ và ông địa lớn. Tuy nhiên, số 78 nhiều người còn hiểu theo hướng xấu có nghĩa là thất bát',
  },
  {
    value: '1797',
    label: 'Con hạc (mang ý nghĩa là Trường thọ)',
  },
  {
    value: '5797',
    label: 'Con hạc (mang ý nghĩa là Trường thọ)',
  },
  {
    value: '2666',
    label: 'Con rồng bay',
  },
  {
    value: '5239',
    label: 'Tiền tài',
  },
  {
    value: '5292',
    label: 'Mã đáo thành công',
  },
  {
    value: '1292',
    label: 'Mã đáo thành công',
  },
  {
    value: '456',
    label: '4 mùa sinh lộc',
  },
  {
    value: '8683',
    label: 'Phát lộc phát tài',
  },
  {
    value: '01234',
    label: 'tay trắng đi lên, 1 vợ, 2 con, 3 tầng, 4 bánh',
  },
  {
    value: '1486 ',
    label: '1 năm 4 mùa phát lộc, lộc phát',
  },
  {
    value: '9279',
    label: '4 mùa sinh lộc',
  },
  {
    value: '3937',
    label: 'Tài trời',
  },
  {
    value: '4953',
    label: '49 chưa qua, 53 đã tới',
  },
  {
    value: '3938',
    label: 'Thần tài, thổ địa',
  },
  {
    value: '569',
    label: 'Phúc lộc thọ',
  },
  {
    value: '3939',
    label: 'Tài lộc',
  },
  {
    value: '227',
    label: 'Vạn vạn tuế',
  },
  {
    value: '151618',
    label: 'Mỗi năm mỗi lộc mỗi phát',
  },
  {
    value: '8386',
    label: 'Phát tài phát lộc',
  },
  {
    value: '181818',
    label: 'Mỗi năm mỗi phát',
  },
  {
    value: '8668',
    label: 'Phát lộc lộc phát',
  },
  {
    value: '191919',
    label: '1 bước lên trời',
  },
  {
    value: '4648',
    label: 'Tứ lộc tứ phát',
  },
  {
    value: '1102',
    label: 'Độc nhất vô nhị',
  },
  {
    value: '8888',
    label: 'Tứ phát',
  },
  {
    value: '2204',
    label: 'Mãi mãi không chết',
  },
  {
    value: '4078',
    label: 'Bốn mùa không thất bát',
  },
  {
    value: '6686',
    label: 'Lộc lộc phát lộc',
  },
  {
    value: '6666',
    label: 'Tứ lộc (4 ông lục)',
  },
  {
    value: '6868',
    label: 'Lộc phát phát lộc',
  },
  {
    value: '3468',
    label: 'Tài tử lộc phát',
  },
  {
    value: '5555',
    label: 'Sinh đường làm ăn',
  },
  {
    value: '6578',
    label: '6 năm thất bát',
  },
  {
    value: '5656',
    label: 'Sinh lộc sinh lộc',
  },
  {
    value: '6868',
    label: 'Lộc phát lộc phát (sáu tấm sáu tấm)',
  },
  {
    value: '0578',
    label: 'Không năm nào thất bát',
  },
  {
    value: '1668',
    label: 'Càng ngày càng phát',
  },
  {
    value: '1111',
    label: 'Tứ trụ vững chắc',
  },
  {
    value: '8686',
    label: 'Phát lộc phát lộc',
  },
  {
    value: '2626',
    label: 'Mãi lộc mãi lộc',
  },
  {
    value: '7308',
    label: 'Thất tài không phát',
  },
  {
    value: '2628',
    label: 'Mãi lộc mãi phát',
  },
  {
    value: '7939',
    label: 'Thần tài LỚN, Thần tài nhỏ',
  },
  {
    value: '1368',
    label: 'Cả một đời lộc phát',
  },
  {
    value: '7838',
    label: 'Ông địa lớn, Ông địa nhỏ',
  },
  {
    value: '1515',
    label: '2 cái rằm',
  },
  {
    value: '7878',
    label: 'Thất bát, thất bát (ông địa)',
  },
  {
    value: '1618',
    label: 'Nhất lộc nhất phát',
  },
  {
    value: '2879',
    label: 'Mãi phát tài',
  },
  {
    value: '1102',
    label: 'Độc nhất vô nhị',
  },
  {
    value: '1122',
    label: 'Một là một, hai là hai (Đệ Nhất Phan Khang đang dùng)',
  },
  {
    value: '6789',
    label: 'Sang bằng tất cả (sống bằng tình cảm)',
  },
  {
    value: '6758',
    label: 'Sống bằng niềm tin',
  },
  {
    value: '0607',
    label: 'Không xấu không bẩn',
  },
  {
    value: '9991',
    label: 'Chửi cha chúng mày',
  },
  {
    value: '9999',
    label: 'Tứ cẩu',
  },
  {
    value: '0378',
    label: 'Phong ba bão táp',
  },
  {
    value: '8181',
    label: 'Phát 1 phát 1',
  },
  {
    value: '3737',
    label: 'Hai ông trời',
  },
  {
    value: '6028',
    label: 'Xấu kô ai táng',
  },
  {
    value: '7762',
    label: 'Bẩn bẩn xấu trai',
  },
]

const tuquy = [
  {
    value: 1,
    label:
      'Theo dân gian, số 1 là căn bản của mọi sự biến hóa, là con số khởi đầu, luôn đem lại những điều mới mẻ, tốt đẹp, đem tới một sức sống mới cho mọi người.',
  },
  {
    value: 2,
    label:
      'Trong các quan niệm xưa, số 2 là con số tượng trưng cho một cặp, một đôi, là con số của hạnh phúc và điều hành thuận lợi cho những sự kiện.Số 2 là “địa thiên thái” chỉ thời vận vận hanh thông, vạn vật được khai mở phát triển, phúc lành đang đến, tượng trưng cho sự cân bằng âm dương.',
  },
  {
    value: 3,
    label:
      'Con số 3 là con số thần bí, có nhiều quan niệm khác nhau, người xưa thường dùng các trạng thái, hình thể gắn với con số 3 như: Tam bảo (Phật – Pháp – Tăng), Tam giới (Dục giới, Sắc giới và Vô sắc giới), Tam thời (Quá khứ – Hiện tại – Vị lai), Tam vô lậu học (Giới– Định – Tuệ), Tam đa (Đa phúc, Đa lộc, Đa thọ), Tam tài (Thiên, Địa, Nhân)…',
  },
  {
    value: 4,
    label:
      'Số 4 là con số linh thiêng. Số 4 không phải là con số xấu theo quan điểm của người Trung Quốc, ngược lại nó là con số đem lại thuận, tốt lành cho chủ nhân của nó. Tập hợp con số 4 trong tứ quý 4444 chắc hẳn sẽ khá hoàn hảo.',
  },
  {
    value: 5,
    label:
      'Số 5 còn là số Vua, thuộc hành Thổ, màu Vàng. Là số trung tâm trong hệ số đếm. Nó là số của những người quyền quý, của sự thăng tiến và sinh sôi, tượng trưng cho danh dự, uy quyền, quyền lực.',
  },
  {
    value: 6,
    label:
      'Trong việc luận số, người ta coi số 6 là đẹp bởi trong cách đọc của hán nôm, số 6 đồng âm với Lộc, vì vậy số 6 là biểu tưởng của Lộc, là tiền lộc, vàng lộc.',
  },
  {
    value: 7,
    label:
      'Theo quan niệm phương Đông, số 7 là con số có sức mạnh kỳ diệu, nó là 7 sao và cùng gươm 7 sao dùng trong nghi lễ đạo Lão. Số 7 tượng trưng cho sức mạnh, đẩy lùi ma quỷ, nó được ban cho một sức mạnh kỳ bí bất khả xâm phạm. Theo đạo Phật số 7 có ý nghĩa là quyền năng mạnh nhất của mặt trời.',
  },
  {
    value: 8,
    label:
      'Số 8 tượng trưng cho sự thịnh vượng, sức khỏe và may mắn. Con số biểu hiện tốt lành này là tám điều bất tử trong đạo Lão và Bát Chánh đạo trong Phật giáo.',
  },
  {
    value: 9,
    label:
      'Biển tứ quý 9999 được xếp vào hạng số 1, vời nhiều từ “trường thọ và may mắn” là con số chính, hạnh phúc an lành và thuận lợi. Cửu là 9, tứ cửu là cứu tử, là trường thọ. Số 9 là con số của hạnh phúc an lành và thuận lợi. Đó là con số tượng trưng cho sự vĩnh cửu đẹp đẽ. Rất nhiều người thích số 9 vì nó gần như là hình ảnh cho sự viên mãn tròn đầy.',
  },
]

const trungquoc = [
  {
    value: '8888',
    label:
      'Đây là loại phổ biến nhất, xuất hiện trong rất nhiều quảng cáo phù hợp với Tết của Trung Quốc. Với số 8 xuất hiện bốn lần, về cơ bản nó có nghĩa là “thịnh vượng, thịnh vượng, thịnh vượng”, rất phù hợp cho những người đang làm kinh doanh vì nó sẽ mang lại cho họ vận may lớn.',
  },
  {
    value: '2828',
    label:
      '2 trong số học Trung Quốc là viết tắt của ‘đôi’, mang lại phước lành vì nhiều người tin rằng những điều tốt đẹp sẽ đến theo từng cặp. Một số người tin rằng nó cũng là viết tắt của “easy”, vì vậy biển số xe này có thể có nghĩa là “dễ dàng thịnh vượng, hai lần”.',
  },
  {
    value: '1688',
    label:
      'Người Trung Quốc tin rằng số 6 (六, liu) là một con số may mắn vì cách phát âm của nó gần giống với 流 (liu), có nghĩa là “chảy” hoặc “tất cả các con đường”. Nhìn chung, biển số xe này có nghĩa là “thịnh vượng mọi cách” hoặc “thịnh vượng suôn sẻ”.',
  },
  {
    value: '2688',
    label:
      'Tương tự với số trên nhưng thay vào đó là số 2. Điều này có thể có nghĩa là “sự thịnh vượng kép trên mọi nẻo đường” hoặc “sự thịnh vượng dễ dàng và suôn sẻ”. Điều này nghe có vẻ là một biển số ô tô tuyệt vời cho những người quá căng thẳng trong công việc nhưng vẫn muốn làm tốt nhất có thể.',
  },
  {
    value: '8338',
    label:
      '3 (三, san) là một con số may mắn khác trong thuật số học Trung Quốc vì nó tương tự như 生 (sheng), có nghĩa là “cuộc sống” hoặc “sinh”. Có cả 3 và 8 trong biển số ô tô của bạn giống như nói rằng “thịnh vượng từ khi sinh ra”, đó sẽ là một biển số xe tuyệt vời nếu bạn có một em bé mới sinh.',
  },
  {
    value: '1328',
    label:
      'Là sự kết hợp của một số con số trên, 1328 là một biển số xe mang ý nghĩa “dễ dàng phát đạt”, là một con số tốt cho những ai có thể đang phải trải qua một số khó khăn. Với con số này, mọi người có thể cảm thấy thư thái hơn ở những thời điểm khác nhau của cuộc đời và tiếp tục một cách dễ dàng.',
  },
  {
    value: '2889',
    label:
      '9 (九, jiu) là một con số may mắn khác đối với nhiều người Trung Quốc vì nó phát âm giống như 久 (jiu), có nghĩa là “dài” trong các phép đo thời gian, hay còn gọi là “lâu dài”. Vì vậy, biển số xe này có nghĩa là “dễ dàng thịnh vượng lâu dài”.',
  },
  {
    value: '8866',
    label: 'Thịnh vượng suôn sẻ, gấp đôi',
  },
]

const haisocuoi = [
  {
    value: '00',
    label: 'Biển số xe 00 có ý nghĩa là không phải lo về kinh tế',
  },
  {
    value: '01',
    label: 'Biển số xe 01 có ý nghĩa là sự độc nhất',
  },
  {
    value: '02',
    label: 'Biển số xe 01 có ý nghĩa là sự độc nhất',
  },
  {
    value: '03',
    label: 'Biển số xe đuôi 03 có ý nghĩa là sự may mắn, tài lộc',
  },
  {
    value: '04',
    label: 'Biển số xe đuôi 04 có ý nghĩa là số tử, con số kém may mắn',
  },
  {
    value: '05',
    label: 'Biển số xe đuôi 05 có ý nghĩa là được phúc đầy',
  },
  {
    value: '06',
    label: 'Biển số xe 06 có ý nghĩa là khởi đầu lộc, tiền tài',
  },
  {
    value: '07',
    label:
      'Biển số xe đuôi 07 có ý nghĩa là sự xui xẻo, sự thất thoát, không tốt',
  },
  {
    value: '08',
    label: 'Biển số xe 08 có ý nghĩa là bắt đầu phát triển',
  },
  {
    value: '09',
    label: 'Biển số xe 09 có ý nghĩa là cân bằng âm dương',
  },
  {
    value: '10',
    label: 'Biển số xe đuôi 10 có ý nghĩa là sự tượng trưng cho sự nuôi dưỡng',
  },
  {
    value: '11',
    label: 'Biển số xe đuôi 11 có ý nghĩa là sự thăng hoa và hy vọng',
  },
  {
    value: '12',
    label: 'Sự tiến lên, phát triển',
  },
  {
    value: '13',
    label: 'Con số xui xẻo không mang lại may mắn',
  },
  {
    value: '14',
    label: 'Số đẹp may mắn với doanh nhân, nhưng không tốt theo dân gian',
  },
  {
    value: '15',
    label: 'Đại diện cho sự phúc thọ',
  },
  {
    value: '16',
    label: 'Nhất lộc',
  },
  {
    value: '17',
    label: 'Lộc tài lộc vượng',
  },
  {
    value: '18',
    label: 'Biển số xe đuôi 18 có ý nghĩa là nhất phát, tức là phát tài',
  },
  {
    value: '19',
    label: 'Biển số xe đuôi 19 có ý nghĩa là một bước lên tiên',
  },
  {
    value: '20',
    label: 'Biển số xe 20 có ý nghĩa là con số của niềm vui',
  },
  {
    value: '21',
    label: 'Sự hài hòa, ổn định',
  },
  {
    value: '22',
    label: 'Sự hạnh phúc, song hỷ',
  },
  {
    value: '23',
    label: 'Đẽo gọt, rụng rơi, hoang vắng',
  },
  {
    value: '24',
    label: 'Sự gắn kết',
  },
  {
    value: '25',
    label: 'Biển số xe 25 có ý nghĩa là sự chi phối mọi thứ xung quanh',
  },
  {
    value: '26',
    label: 'Mãi lộc',
  },
  {
    value: '27',
    label: 'Biển số xe đuôi 27 có ý nghĩa là dễ làm ăn',
  },
  {
    value: '28',
    label: 'Sự hoàn hảo, hướng tới điều hoàn mỹ',
  },
  {
    value: '29',
    label: 'Mãi mãi, vĩnh cửu',
  },
  {
    value: '30',
    label:
      'Biển số xe 30 có ý nghĩa là sự may hay rủi, sự cô đơn nhưng may mắn',
  },
  {
    value: '31',
    label: 'Sự khởi đầu vững chắc',
  },
  {
    value: '32',
    label: 'Tượng trưng cho tính cân đối',
  },
  {
    value: '33',
    label: 'Biển số xe 32 có ý nghĩa là sự vững chắc, kiên định',
  },
  {
    value: '34',
    label: 'Mang tính tích cực',
  },
  {
    value: '35',
    label: 'Sự hòa hợp, không xui cũng không may',
  },
  {
    value: '36',
    label: 'Dễ dàng sinh lộc',
  },
  {
    value: '37',
    label: 'Gia đình hòa thuận vững bền',
  },
  {
    value: '38',
    label: 'Ông Địa nhỏ, con số may mắn',
  },
  {
    value: '39',
    label: 'Thần tài nhỏ, mang đến tài lộc',
  },
  {
    value: '40',
    label: 'Biển số xe 40 có ý nghĩa là lôi Thủy Giải, sự thoát nạn',
  },
  {
    value: '41',
    label: 'Sự giảm sút',
  },
  {
    value: '42',
    label: 'Sự tăng lên, vượt lên',
  },
  {
    value: '43',
    label: 'Sự thâm nhập',
  },
  {
    value: '44',
    label: 'Sự chết chóc, kết thúc',
  },
  {
    value: '45',
    label: 'Sự tập họp đám đông',
  },
  {
    value: '46',
    label: 'Đẩy lên cao',
  },
  {
    value: '47',
    label: 'Khốn cùng, bất lực',
  },
  {
    value: '48',
    label: 'Sự tương trợ',
  },
  {
    value: '49',
    label: 'Biểu thị sư kém may mắn',
  },
  {
    value: '50',
    label: 'Sự rèn luyện, trật tự',
  },
  {
    value: '51',
    label: 'Biển số xe 51 có ý nghĩa là sự chuyển động, biến động',
  },
  {
    value: '52',
    label: 'Bất động, ngưng trệ',
  },
  {
    value: '53',
    label: 'Sinh tài hoặc vận hạn tùy vào cách nhìn nhận',
  },
  {
    value: '54',
    label: 'Trẻ trung tươi mới',
  },
  {
    value: '55',
    label: 'May mắn, phúc đức',
  },
  {
    value: '56',
    label: 'Sinh lộc, may mắn',
  },
  {
    value: '57',
    label: 'Sự đồng lòng',
  },
  {
    value: '58',
    label: 'Phát triển lớn mạnh',
  },
  {
    value: '59',
    label: 'Biển số xe đuôi 59 có ý nghĩa là sinh trưởng mãi mãi',
  },
  {
    value: '60',
    label: 'Điều tồi tệ đen tối đã qua',
  },
  {
    value: '61',
    label: 'Sự thành thật, có uy tín',
  },
  {
    value: '62',
    label: 'Lộc mãi, số đẹp',
  },
  {
    value: '63',
    label: 'Lợi ích, hợp tác cùng nhau',
  },
  {
    value: '64',
    label: 'Sự dang dở, còn tiếp nối',
  },
  {
    value: '65',
    label: 'Lộc sinh, phú quý trường thọ',
  },
  {
    value: '66',
    label: 'Song lộc, số may mắn',
  },
  {
    value: '67',
    label: 'Sự không may, không mang lại điều tốt',
  },
  {
    value: '68',
    label: 'Lộc phát, mang đến tiền tài cho chủ sở hữu',
  },
  {
    value: '69',
    label: 'Nguồn lộc dồi dào, bất tận',
  },
  {
    value: '70',
    label: 'Sự an nhàn, thịnh vượng',
  },
  {
    value: '71',
    label: 'Sự ăn chơi hưởng thụ',
  },
  {
    value: '72',
    label: 'Bệnh tật, sự đau yếu',
  },
  {
    value: '73',
    label: 'Điều không may mắn',
  },
  {
    value: '74',
    label: 'Sự hưng vượng, lộc trời ban',
  },
  {
    value: '75',
    label: 'Dê già, dê cụ',
  },
  {
    value: '76',
    label: 'Sự toại ngoại trong danh lợi',
  },
  {
    value: '77',
    label: 'Thiên thời',
  },
  {
    value: '78',
    label: 'Thất bát, số xấu nên tránh',
  },
  {
    value: '79',
    label: 'Quyền năng lớn nhất',
  },
  {
    value: '80',
    label: 'Biển số xe đuôi 80 có ý nghĩa là tình cảm nồng nàn',
  },
  {
    value: '81',
    label: 'Sự phát triển, tiến lên',
  },
  {
    value: '82',
    label: 'Mãi phát triển',
  },
  {
    value: '83',
    label: 'Phát tài',
  },
  {
    value: '84',
    label: 'Làm ăn phát đạt',
  },
  {
    value: '85',
    label: 'Phát đạt, hưng thịnh',
  },
  {
    value: '86',
    label: 'Phát lộc, mang đến của cải',
  },
  {
    value: '87',
    label: 'Hao tổn tiền bạc',
  },
  {
    value: '88',
    label: 'Số đại phát',
  },
  {
    value: '89',
    label: 'Phát trường cửu, sự giàu sang',
  },
  {
    value: '90',
    label: 'Sự nguy khốn, vất vả',
  },
  {
    value: '91',
    label: 'Làm ăn luôn gặp khó khăn',
  },
  {
    value: '92',
    label: 'Quyền lực, vĩnh cửu',
  },
  {
    value: '93',
    label: 'Sự lo lắng, buồn rầu',
  },
  {
    value: '94',
    label: 'Làm ăn tiến triển đúng kế hoạch',
  },
  {
    value: '95',
    label: 'Đa mưu và thủ đoạn',
  },
  {
    value: '96',
    label: 'Sự dèm pha',
  },
  {
    value: '97',
    label: 'Biển hiện sự trường thọ',
  },
  {
    value: '98',
    label: 'Phát đạt mãi, may mắn cho chủ sở hữu',
  },
  {
    value: '99',
    label: 'Biển số xe 99 có ý nghĩa là sức mạnh, thời gian vĩnh cửu',
  },
]

// 10
const menhcan = [
  {
    value: 1,
    label: [4, 5],
  },
  {
    value: 2,
    label: [6, 7],
  },
  {
    value: 3,
    label: [8, 9],
  },
  {
    value: 4,
    label: [0, 1],
  },
  {
    value: 5,
    label: [2, 3],
  },
]

// 12
const menhchi = [
  {
    value: 0,
    label: [0, 1, 6, 7],
  },
  {
    value: 1,
    label: [2, 3, 9, 8],
  },
  {
    value: 2,
    label: [4, 5, 10, 11],
  },
]

const can = [
  {
    value: 0,
    label: 'Canh',
  },
  {
    value: 1,
    label: 'Tân',
  },
  {
    value: 2,
    label: 'Nhâm',
  },
  {
    value: 3,
    label: 'Quý',
  },
  {
    value: 4,
    label: 'Giáp',
  },
  {
    value: 5,
    label: 'Ất',
  },
  {
    value: 6,
    label: 'Bính',
  },
  {
    value: 7,
    label: 'Đinh',
  },
  {
    value: 8,
    label: 'Mậu',
  },
  {
    value: 9,
    label: 'Kỷ',
  },
]

const chi = [
  {
    value: 0,
    label: 'Tý',
  },
  {
    value: 1,
    label: 'Sửu',
  },
  {
    value: 2,
    label: 'Dần',
  },
  {
    value: 3,
    label: 'Mão',
  },
  {
    value: 4,
    label: 'Thìn',
  },
  {
    value: 5,
    label: 'Tỵ',
  },
  {
    value: 6,
    label: 'Ngọ',
  },
  {
    value: 7,
    label: 'Mùi',
  },
  {
    value: 8,
    label: 'Thân',
  },
  {
    value: 9,
    label: 'Dậu',
  },
  {
    value: 10,
    label: 'Tuất',
  },
  {
    value: 11,
    label: 'Hợi',
  },
]

// Vi dụ thien can theo nam sinh 1890 = Canh Ngọ
// can:  0 = Canh
// menhcan: 4
// chi: 1890/ 12 dư 6 -> Ngọ
// menhchi: 0
// Mệnh = 4 + 0 = 4 -> Thổ

const menhmau = [
  {
    menh: 1,
    tuong_sinh: [1, 2],
    hoa_hop: [3, 4, 5],
    che_khac: [6],
    bi_khac: [7, 8, 9],
  },
  {
    menh: 5,
    tuong_sinh: [10, 11],
    hoa_hop: [6],
    che_khac: [1, 2],
    bi_khac: [3, 4, 5],
  },
  {
    menh: 2,
    tuong_sinh: [3, 4, 5],
    hoa_hop: [10, 11],
    che_khac: [7, 8, 9],
    bi_khac: [1, 2],
  },
  {
    menh: 3,
    tuong_sinh: [6],
    hoa_hop: [7, 8, 9],
    che_khac: [3, 4, 5],
    bi_khac: [10, 11],
  },
  {
    menh: 4,
    tuong_sinh: [7, 8, 9],
    hoa_hop: [1, 2],
    che_khac: [10, 11],
    bi_khac: [6],
  },
]

const menhColor = [
  {
    id: 1,
    name: 'Vàng',
    color: '#FFFF00',
  },
  {
    id: 2,
    name: 'Nâu đất',
    color: '#964B00',
  },
  {
    id: 3,
    name: 'Trắng',
    color: '#FFFFFF',
  },
  {
    id: 4,
    name: 'Xám',
    color: '#C0C0C0',
  },
  {
    id: 5,
    name: 'Ghi',
    color: '#808080',
  },
  {
    id: 6,
    name: 'Xanh Lục',
    color: '#50C878',
  },
  {
    id: 7,
    name: 'Đỏ',
    color: '#FF0000',
  },
  {
    id: 8,
    name: 'Hồng',
    color: '#FF00FF',
  },
  {
    id: 9,
    name: 'Tím',
    color: '#660099',
  },
  {
    id: 10,
    name: 'Đen',
    color: '#000000',
  },
  {
    id: 11,
    name: 'Xanh biển',
    color: '#0000FF',
  },
]

const quai = [
  {
    value: 1,
    name: 'Càn',
    que: '111',
  },
  {
    value: 2,
    name: 'Đoài',
    que: '011',
  },
  {
    value: 3,
    name: 'Ly',
    que: '101',
  },
  {
    value: 4,
    name: 'Chấn',
    que: '001',
  },
  {
    value: 5,
    name: 'Tốn',
    que: '110',
  },
  {
    value: 6,
    name: 'Khảm',
    que: '010',
  },
  {
    value: 7,
    name: 'Cấn',
    que: '100',
  },
  {
    value: 8,
    name: 'Khôn',
    que: '000',
  },
]

const que64 = [
  {
    value: 1,
    name: 'Thuần càn',
    ud: [1, 1],
    type: 'Đại cát',
    dich: 'Đầu cả, hanh thông, lợi tốt, chính bền',
  },
  {
    value: 14,
    name: 'Hỏa thiên đại hữu',
    ud: [3, 1],
    type: 'Đại cát',
    dich: 'Quẻ đại hữu cá lơn hanh thông',
  },
  {
    value: 26,
    name: 'Sơn tiên đại súc',
    ud: [7, 1],
    type: 'Đại cát',
    dich: 'Đầu cả, hanh thông, lợi tốt, chính bền',
  },
  {
    value: 9,
    name: 'Phong thiên tiểu xúc',
    ud: [5, 1],
    type: 'Bình hòa',
    dich: 'Chứa nhỏ hanh thông, mây dày không mưa, tự cõi tây ta',
  },
  {
    value: 11,
    name: 'Địa thiên thái',
    ud: [8, 1],
    type: 'Cát hanh',
    dich: 'Quẻ thái, nhỏ đi, lớn lại, lành tốt hanh thông',
  },
  {
    value: 5,
    name: 'Thủy thiên nhu',
    ud: [6, 1],
    type: 'Cát',
    dich: 'Quẻ Nhu, có đức tín, sáng láng, hanh thông, chính bền, tốt! Lợi sang sông lớn',
  },
  {
    value: 43,
    name: 'Trạch thiên quải',
    ud: [2, 1],
    type: 'Vô du lợi (không có lợi)',
    dich: 'Quẻ Quải, giơ chưng sân vua, tin gọi, có nguy. Bảo từ làng, chẳng lợi tới quân, lợi có thửa đi',
  },
  {
    value: 34,
    name: 'Lôi thiên đại tráng',
    ud: [4, 1],
    type: 'Lợi',
    dich: 'Quẻ Đại tráng lợi về sự chính',
  },
  {
    value: 13,
    name: 'Thiên hỏa đồng nhân',
    ud: [1, 3],
    type: 'Cát',
    dich: 'Cùng người ở đồng, hanh thông, lợi về sự sang sông lờn, lợi cho sự chính bền của đấng quân tử',
  },
  {
    value: 30,
    name: 'Thuần ly',
    ud: [3, 3],
    type: 'Hanh',
    dich: 'Quẻ Ly lợi về sự chính, hanh, nuôi trâu cái, tốt',
  },
  {
    value: 22,
    name: 'Sơn hỏa bí',
    ud: [7, 3],
    type: 'Cát',
    dich: 'Quẻ Bí danh, hơi lợi có thửa đi',
  },
  {
    value: 37,
    name: 'Phong hỏa gia nhân',
    ud: [5, 3],
    type: 'Cát',
    dich: 'Quẻ Gia nhân lợi về gái chính',
  },
  {
    value: 36,
    name: 'Địa hỏa minh di',
    ud: [8, 3],
    type: 'Hung',
    dich: 'Quẻ Minh di lợi về khó nhọc trinh chính',
  },
  {
    value: 63,
    name: 'Thủy hỏa ký tế',
    ud: [6, 3],
    type: 'Cát hanh',
    dich: 'Quẻ Ký Tế hanh, nhỏ lợi về chính bền, đầu tốt',
  },
  {
    value: 49,
    name: 'Trạch hỏa cách',
    ud: [2, 3],
    type: 'Cát',
    dich: 'Quẻ Cách, hết ngày bèn tin, cả hanh lợi trinh',
  },
  {
    value: 55,
    name: 'Lôi hỏa phong',
    ud: [4, 3],
    type: 'Cát hanh',
    dich: 'Quẻ Phong hanh thông, vua đến đấy, chớ lo, nên mặt trời giữa',
  },
  {
    value: 33,
    name: 'Thiên sơn độn',
    ud: [1, 7],
    type: 'Vô cữu (không lỗi)',
    dich: 'Quẻ Độn hanh, nhỏ lợi trinh',
  },
  {
    value: 56,
    name: 'Hỏa sơn lữ',
    ud: [3, 7],
    type: 'Bình hòa (Vô cữu - không lỗi)',
    dich: 'Quẻ Lữ nhỏ hanh thông sự đi đường chính bền tốt',
  },
  {
    value: 52,
    name: 'Thuần cấn',
    ud: [7, 7],
    type: 'Bình hòa (Vô cữu - Không lỗi)',
    dich: 'Đậu thửa lưng, chẳng được thửa mình, đi thửa sân, chẳng thấy thửa người, không lỗi',
  },
  {
    value: 53,
    name: 'Phong sơn tiệm',
    ud: [5, 7],
    type: 'Cát',
    dich: 'Quẻ Tiệm, con gái về, tốt, lợi về chính bền',
  },
  {
    value: 15,
    name: 'Địa sơn khiêm',
    ud: [8, 7],
    type: 'Cát',
    dich: 'Quẻ Khiêm hanh thông, đấng quân tử có sau chót',
  },
  {
    value: 39,
    name: 'Thủy sơn kiển',
    ud: [6, 7],
    type: 'Hung',
    dich: 'Quẻ Kiển, lợi Tây Nam không lợi Đông Bắc, lợi về sự thấy người lớn, chính thì tốt',
  },
  {
    value: 31,
    name: 'Trạch sơn hàm',
    ud: [2, 7],
    type: 'Cát hanh',
    dich: 'Quẻ Hàm hanh, lợi chính, lấy con gái, tốt',
  },
  {
    value: 62,
    name: 'Lôi sơn tiểu quá',
    ud: [4, 7],
    type: 'Bình hòa (vô cữu - Không lỗi)',
    dich: 'Quẻ Tiểu Quá hanh, lợi về chính bền',
  },
  {
    value: 44,
    name: 'Thiên phong cấu',
    ud: [1, 5],
    type: 'Bình Hòa (vô Cữu - Không lỗi)',
    dich: 'Quẻ Cấu, con gái mạnh, chớ dùng lấy con gái',
  },
  {
    value: 50,
    name: 'Hỏa phong đỉnh',
    ud: [3, 5],
    type: 'Cát hanh',
    dich: 'Quẻ Đỉnh, cả tốt hanh',
  },
  {
    value: 18,
    name: 'Sơn phong cổ',
    ud: [7, 5],
    type: 'Hung',
    dich: 'Quẻ Cổ cả, hanh, lợi về sang sông lớn. Trước giáp ba ngày, sau giáp ba ngày',
  },
  {
    value: 57,
    name: 'Thuần tốn',
    ud: [5, 5],
    type: 'Bình hòa',
    dich: 'Quẻ Tốn nhỏ hanh thông, lợi có thửa đi, lợi thấy người lớn',
  },
  {
    value: 46,
    name: 'Địa phong thăng',
    ud: [8, 5],
    type: 'Cát hanh',
    dich: 'Quẻ Thăng, cả hanh, dùng thấy người lớn, chớ lo, đi về phương Nam tốt',
  },
  {
    value: 48,
    name: 'Thủy phong tỉnh',
    ud: [6, 5],
    type: 'Bình hòa (Vô Hối - Không hối tiếc)',
    dich: 'Quẻ Tỉnh, đổi làng chẳng đổi giếng, không mất không được, đi lại giếng giếng',
  },
  {
    value: 28,
    name: 'Trạch phong đại quá',
    ud: [2, 5],
    type: 'Lợi',
    dich: 'Quẻ Đại Quá, cột ỏe, lợi có thửa đi, hanh',
  },
  {
    value: 32,
    name: 'Lôi phong hằng',
    ud: [4, 5],
    type: 'Cát',
    dich: 'Quẻ Hằng hanh, lợi về sự chính, lợi có thửa đi',
  },
  {
    value: 12,
    name: 'Thiên địa bĩ',
    ud: [1, 8],
    type: 'Hung',
    dich: 'Bĩ đấy, chẳng phải người. Chẳng lợi cho sự chính bền của đấng quân tử, lớn đi nhỏ lại',
  },
  {
    value: 35,
    name: 'Hỏa địa tấn',
    ud: [3, 8],
    type: 'Cát hanh',
    dich: 'Quẻ Tấn, tước hầu yên dùng cho ngựa giậm nhiều, ban ngày ba lần tiếp',
  },
  {
    value: 23,
    name: 'Sơn địa bác',
    ud: [7, 8],
    type: 'Hung',
    dich: 'Quẻ Bác không lợi có thửa đi',
  },
  {
    value: 20,
    name: 'Phong địa quan',
    ud: [5, 8],
    type: 'Bình hòa',
    dich: 'Quẻ quan, rửa mà không cứng, có tin, dường cung kính vậy',
  },
  {
    value: 2,
    name: 'Thuần khôn',
    ud: [8, 8],
    type: 'Đại cát',
    dich: 'Quẻ Khôn: đầu cả, hanh thông, lợi về nết trinh của ngựa cái. Quân tử có sự đi. Trước mê, sau được. Chủ về lợi. Phía tây nam được bạn, phía Đông Bắc mất bạn. Yên phận giữ nết trinh thì tốt',
  },
  {
    value: 8,
    name: 'Thủy địa tỷ',
    ud: [6, 8],
    type: 'Cát',
    dich: 'Liền nhau tốt, truy nguyên việc bói, đầu cả, lâu dài, chính bền, không lỗi',
  },
  {
    value: 45,
    name: 'Trạch địa tụy',
    ud: [2, 8],
    type: 'Hanh (hanh thông - thuận lợi)',
    dich: 'Quẻ Tuy hanh, vua đền có miếu',
  },
  {
    value: 16,
    name: 'Lôi địa dự',
    ud: [4, 8],
    type: 'Lợi',
    dich: 'Quẻ Dự, lợi cho sự dựng nước hầu, trẩy quân',
  },
  {
    value: 6,
    name: 'Thiên thủy tụng',
    ud: [1, 6],
    type: 'Hung',
    dich: 'Kiện, có thật, bị lấp, phải Sợ, vừa phải, tốt; theo đuổi đến chót, xấu; lợi về sự thấy người lớn, không lợi về sự sang sông lớn',
  },
  {
    value: 64,
    name: 'Hỏa thủy vị tế',
    ud: [3, 6],
    type: 'Vô du lợi (không có lợi)',
    dich: 'Quẻ Vị Tế hanh, con cáo nhỏ hầu sang, ướt thửa đuôi, không thửa lợi',
  },
  {
    value: 4,
    name: 'Sơn thủy mông',
    ud: [7, 6],
    type: 'Cát',
    dich: 'Cùng người ở đồng, hanh thông, lợi về sự sang sông lờn, lợi cho sự chính bền của đấng quân tử',
  },
  {
    value: 59,
    name: 'Phong thủy hoán',
    ud: [5, 6],
    type: 'Hanh',
    dich: 'Quẻ Hoán hanh, vua đến có miếu, lợi sang sông lớn, lợi về chính bền',
  },
  {
    value: 7,
    name: 'Địa thủy sư',
    ud: [8, 6],
    type: 'Cát',
    dich: 'Quân chính, bậc trượng nhân tốt, không lỗi',
  },
  {
    value: 29,
    name: 'Thuần khảm',
    ud: [6, 6],
    type: 'Bình Hòa (vô hối)',
    dich: 'Quẻ Khảm kép, có tin, bui lòng, hanh, đi có chuộng',
  },
  {
    value: 47,
    name: 'Trạch thủy khốn',
    ud: [2, 6],
    type: 'Bình Hòa (Vô cữu - Không lỗi)',
    dich: 'Quẻ Khốn hanh, chính bền, người lớn tốt , không lỗi',
  },
  {
    value: 40,
    name: 'Lôi thủy giải',
    ud: [],
    type: 'Cát hanh',
    dich: 'Quẻ Giải lợi về phương Tây Nam, không thửa đi, thì lại lại; có thửa đi, sớm thì tốt',
  },
  {
    value: 10,
    name: 'Thiên trạch lý',
    ud: [1, 2],
    type: 'Cát',
    dich: 'Xéo đuôi cọp, không cắn người, hanh',
  },
  {
    value: 38,
    name: 'Hỏa trạch khuê',
    ud: [3, 2],
    type: 'Hung',
    dich: 'Quẻ Khuê, việc nhỏ tốt',
  },
  {
    value: 41,
    name: 'Sơn trạch tổn',
    ud: [7, 2],
    type: 'Bình Hòa (Vô cữu - Không lỗi)',
    dich: 'Quẻ Tốn, có tin, cả tốt, không lỗi, khá trinh, lợi',
  },
  {
    value: 61,
    name: 'Phong trạch trung phu',
    ud: [5, 2],
    type: 'Cát hanh',
    dich: 'Quẻ Trung phu, cá lợn tốt, lợi sang sông lớn, lợi về chính bền',
  },
  {
    value: 19,
    name: 'Địa trạch lâm',
    ud: [8, 2],
    type: 'Vô Hối (Cẩn trọng, phòng ngừa rủi ro thì không hối hận)',
    dich: 'Quẻ Lâm, cả, hanh, lợi, trinh, đến chưng tám tháng, có hung',
  },
  {
    value: 60,
    name: 'Thủy trạch tiết',
    ud: [6, 2],
    type: 'Cát',
    dich: 'Quẻ Tiết hanh, sự dè dặt khổ không thể chính bền',
  },
  {
    value: 58,
    name: 'Thuần đoài',
    ud: [2, 2],
    type: 'Cát hanh',
    dich: 'Quẻ Đoài hanh, lợi về chính bền',
  },
  {
    value: 54,
    name: 'Lôi trạch quy muội',
    ud: [4, 2],
    type: 'Hung',
    dich: 'Quẻ Qui Muội đi hung không thửa lợi',
  },
  {
    value: 25,
    name: 'Thiên lôi vô vụng',
    ud: [1, 4],
    type: 'Vô lợi (không tốt)',
    dich: 'Quẻ Vô Vọng cả hanh, lợi về sự chính bền; thửa chẳng chính có tội, không lợi có thửa đi',
  },
  {
    value: 21,
    name: 'Hỏa lôi phệ hạp',
    ud: [3, 4],
    type: 'Bình hòa',
    dich: 'Quẻ Phệ hạp hanh, lợi dùng việc ngục',
  },
  {
    value: 27,
    name: 'Sơn lôi đi',
    ud: [7, 4],
    type: 'Cát hanh',
    dich: 'Quẻ Di, chính tốt, xem sự nuôi, tự tìm cái thật của miệng',
  },
  {
    value: 42,
    name: 'Phong lôi ích',
    ud: [5, 4],
    type: 'Cát',
    dich: 'Quẻ ích lợi có thửa đi, lợi về sang sông lớn',
  },
  {
    value: 24,
    name: 'Địa lôi phục',
    ud: [8, 4],
    type: 'Bình hòa (vô cữu - Không lỗi)',
    dich: 'Quẻ Phục hanh, ra vào không tật, bạn đến không lỗi',
  },
  {
    value: 3,
    name: 'Thủy lôi truân',
    ud: [6, 4],
    type: 'Hung',
    dich: 'Truân đầu cả, hanh thông, lợi tốt, chính bền, chớ dùng có thửa đi, lợi về dựng tước hầu',
  },
  {
    value: 17,
    name: 'Trạch lôi tùy',
    ud: [2, 4],
    type: 'Bình hòa (vô cữu - Không lỗi)',
    dich: 'Quẻ Tuỳ cả, lợi, trinh, không lỗi',
  },
  {
    value: 51,
    name: 'Thuần chấn',
    ud: [4, 4],
    type: 'Bình hòa (Vô cữu - Không lỗi)',
    dich: 'Quẻ Chấn hanh, sợ lại ngơm ngớp, cười nói khanh khách, nhức kinh trăm dặm, chẳng mất môi và rượu Xưởng',
  },
]

export default {
  tong,
  phongThuy80,
  dayso,
  tuquy,
  trungquoc,
  haisocuoi,
  can,
  chi,
  menh,
  menhcan,
  menhchi,
  menhColor,
  menhmau,
  nguhanh,
  quai,
  que64,
  nguhanhsinh,
  nguhanhkhac,
  tuongquanhanhxe,
  hanhbsx,
}
