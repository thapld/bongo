import React from 'react'
import { Fontisto, Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'
import { Platform, StyleSheet } from 'react-native'
import { sizes } from '../theme'

const news1 = {
  id: 1,
  description:
    'Nhắc đến Rolls-Royce Phantom tại Việt Nam chắc hẳn không ai sở hữu nhiều xe bằng đại gia này',
  image: require('../../assets/dummyData/news1.jpg'),
}

const news2 = {
  id: 2,
  description:
    'Khám phá Audi R8 V10 Plus biển đẹp của hot girl 9X được nhà chồng đại gia tặng làm quà cưới',
  image: require('../../assets/dummyData/news2.jpg'),
}

const news3 = {
  id: 3,
  description:
    'Kia Sonet biển số ngũ quý 9 tại Nghệ An về tay chủ mới với giá 1,6 tỷ đồng',
  image: require('../../assets/dummyData/news3.jpg'),
}

const news4 = {
  id: 4,
  description:
    'Bắt trend thời "trẻ trâu", Phan Thành chia sẻ hình ảnh tuổi 18 với chuẩn thiếu gia khi toàn siêu xe đắt tiền',
  image: require('../../assets/dummyData/news4.jpg'),
}

const news5 = {
  id: 5,
  description:
    'Xem trước thiết kế ôtô điện Chrysler qua mẫu Chrysler Airflow Graphite',
  image: require('../../assets/dummyData/news5.jpg'),
}

const news6 = {
  id: 6,
  description:
    'Bắt trend thời "trẻ trâu", Phan Thành chia sẻ hình ảnh tuổi 18 với chuẩn thiếu gia khi toàn siêu xe đắt tiền',
  image: require('../../assets/dummyData/news6.jpg'),
}

const news7 = {
  id: 7,
  description:
    'Doanh số SUV cỡ A tháng 2/2022: Toyota Raize vượt qua KIA Sonet',
  image: require('../../assets/dummyData/news7.jpg'),
}

const newsData = [news1, news2, news3, news4, news5, news6, news7]

const menu = [
  {
    id: 1,
    name: 'Featured',
  },
  {
    id: 2,
    name: 'Nearby you',
  },
  {
    id: 3,
    name: 'Popular',
  },
  {
    id: 4,
    name: 'Newest',
  },
  {
    id: 5,
    name: 'Trending',
  },
  {
    id: 6,
    name: 'Recommended',
  },
]

const signals = [
  {
    id: 1,
    image: require('../../assets/images/learning/image_450/41_.webp'),
    title: 'Cấm xe xúc vật kéo',
  },
  {
    id: 2,
    image: require('../../assets/images/learning/image_450/36_.webp'),
    title: 'Vạch nằm ngang 19',
  },
  {
    id: 3,
    image: require('../../assets/images/learning/image_450/25_.webp'),
    title: 'Biển chỉ dẫn số hiệu lối ra',
  },
  {
    id: 4,
    image: require('../../assets/images/learning/image_450/32_.webp'),
    title: 'Hướng của các biển',
  },
  {
    id: 5,
    image: require('../../assets/images/learning/image_450/343_.webp'),
    title: 'Vạch nằm đứng 4',
  },
  {
    id: 6,
    image: require('../../assets/images/learning/image_450/304_.webp'),
    title: 'Vạch nằm ngang 12',
  },
  {
    id: 7,
    image: require('../../assets/images/learning/image_600/q355.webp'),
    title: 'Biển chỉ dẫn số hiệu lối ra',
  },
  {
    id: 8,
    image: require('../../assets/images/learning/image_600/q430.webp'),
    title: 'Dốc xuống nguy hiểm',
  },
  {
    id: 9,
    image: require('../../assets/images/learning/image_450/26_.webp'),
    title: 'Hết đường dành cho oto',
  },
  {
    id: 10,
    image: require('../../assets/images/learning/image_450/24_.webp'),
    title: 'Cầu xoay - cầu cất',
  },
]

const quiz = {
  id: 201,
  question:
    '201. Người lái xe có văn hóa khi tham gia giao thông phải đáp ứng các yêu cầu nào sau đây?',
  answers: [
    {
      id: 1,
      label: 'A',
      text: 'Có trách nhiệm với bản thân và với cộng đồng, tôn trọng, nhường nhịn người khác.',
    },
    {
      id: 2,
      label: 'B',
      text: 'Tận tình giúp đỡ người tham gia giao thông gặp hoạn nạn, giúp đỡ người khuyết tật, trẻ em và người cao tuổi.',
    },
    {
      id: 3,
      label: 'C',
      text: 'Cả ý 1 và ý 2.',
    },
  ],
}

const violations = [
  {
    id: 1,
    licensePlate: '30A34567',
    licensePlateColor: 'Nền chữ màu trắng và chứ số màu đen',
    vehicleType: 'Ô tô',
    violationTime: '2022/02/08',
    violationLocation: 'Nội Bài, Lào Cai',
    status: 'Chưa xử phạt',
    provider: 'Đội TTKSGTĐB số 1, phòng 8 - Cục cảnh sát giao thông',
    contractMobile: '02435816399',
  },
  {
    id: 2,
    licensePlate: '30A34567',
    licensePlateColor: 'Nền chữ màu trắng và chứ số màu đen',
    vehicleType: 'Ô tô',
    violationTime: '2022/03/10',
    violationLocation: 'Pháp Vân - Cầu Giẽ',
    status: 'Chưa xử phạt',
    provider: 'Đội TTKSGTĐB số 1, phòng 8 - Cục cảnh sát giao thông',
    contractMobile: '02435816399',
  },
]

const { width } = sizes
const CARD_HEIGHT = 220
const CARD_WIDTH = sizes.width * 0.8

const Images = [
  { image: require('../../assets/dummyData/banners/food-banner1.jpg') },
  { image: require('../../assets/dummyData/banners/food-banner2.jpg') },
  { image: require('../../assets/dummyData/banners/food-banner3.jpg') },
  { image: require('../../assets/dummyData/banners/food-banner4.jpg') },
]

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchBox: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 50 : 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 5,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  chipsScrollView: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 90 : 80,
    paddingHorizontal: 10,
  },
  chipsIcon: {
    marginRight: 5,
  },
  chipsItem: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 8,
    paddingHorizontal: 20,
    marginHorizontal: 10,
    height: 35,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  scrollView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    // padding: 10,
    elevation: 2,
    backgroundColor: '#FFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: 'hidden',
  },
  cardImage: {
    flex: 3,
    width: '100%',
    height: '100%',
    alignSelf: 'center',
  },
  textContent: {
    flex: 2,
    padding: 10,
  },
  cardtitle: {
    fontSize: 12,
    // marginTop: 5,
    fontWeight: 'bold',
  },
  cardDescription: {
    fontSize: 12,
    color: '#444',
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
  },
  marker: {
    width: 30,
    height: 30,
  },
  button: {
    alignItems: 'center',
    marginTop: 5,
  },
  signIn: {
    width: '100%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  textSign: {
    fontSize: 14,
    fontWeight: 'bold',
  },
})

const markers = [
  {
    coordinate: {
      latitude: 22.6293867,
      longitude: 88.4354486,
    },
    title: 'Amazing Food Place',
    description: 'This is the best food place',
    image: Images[0].image,
    rating: 4,
    reviews: 99,
  },
  {
    coordinate: {
      latitude: 22.6345648,
      longitude: 88.4377279,
    },
    title: 'Second Amazing Food Place',
    description: 'This is the second best food place',
    image: Images[1].image,
    rating: 5,
    reviews: 102,
  },
  {
    coordinate: {
      latitude: 22.6281662,
      longitude: 88.4410113,
    },
    title: 'Third Amazing Food Place',
    description: 'This is the third best food place',
    image: Images[2].image,
    rating: 3,
    reviews: 220,
  },
  {
    coordinate: {
      latitude: 22.6341137,
      longitude: 88.4497463,
    },
    title: 'Fourth Amazing Food Place',
    description: 'This is the fourth best food place',
    image: Images[3].image,
    rating: 4,
    reviews: 48,
  },
  {
    coordinate: {
      latitude: 22.6292757,
      longitude: 88.444781,
    },
    title: 'Fifth Amazing Food Place',
    description: 'This is the fifth best food place',
    image: Images[3].image,
    rating: 4,
    reviews: 178,
  },
]

const mapDarkStyle = [
  {
    elementType: 'geometry',
    stylers: [
      {
        color: '#212121',
      },
    ],
  },
  {
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#212121',
      },
    ],
  },
  {
    featureType: 'administrative',
    elementType: 'geometry',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
  {
    featureType: 'administrative.land_parcel',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'administrative.locality',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#bdbdbd',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [
      {
        color: '#181818',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#616161',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#1b1b1b',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#2c2c2c',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#8a8a8a',
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'geometry',
    stylers: [
      {
        color: '#373737',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [
      {
        color: '#3c3c3c',
      },
    ],
  },
  {
    featureType: 'road.highway.controlled_access',
    elementType: 'geometry',
    stylers: [
      {
        color: '#4e4e4e',
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#616161',
      },
    ],
  },
  {
    featureType: 'transit',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#000000',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#3d3d3d',
      },
    ],
  },
]

const mapStandardStyle = [
  {
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
]

const initialMapState = {
  markers,
  categories: [
    {
      name: 'Fastfood Center',
      icon: (
        <MaterialCommunityIcons
          style={styles.chipsIcon}
          name="food-fork-drink"
          size={18}
        />
      ),
    },
    {
      name: 'Restaurant',
      icon: (
        <Ionicons name="ios-restaurant" style={styles.chipsIcon} size={18} />
      ),
    },
    {
      name: 'Dineouts',
      icon: (
        <Ionicons name="md-restaurant" style={styles.chipsIcon} size={18} />
      ),
    },
    {
      name: 'Snacks Corner',
      icon: (
        <MaterialCommunityIcons
          name="food"
          style={styles.chipsIcon}
          size={18}
        />
      ),
    },
    {
      name: 'Hotel',
      icon: <Fontisto name="hotel" style={styles.chipsIcon} size={15} />,
    },
  ],
  region: {
    latitude: 22.62938671242907,
    longitude: 88.4354486029795,
    latitudeDelta: 0.04864195044303443,
    longitudeDelta: 0.040142817690068,
  },
}

export default {
  newsData,
  menu,
  signals,
  quiz,
  violations,
  initialMapState,
  markers,
  mapDarkStyle,
  mapStandardStyle,
  CARD_HEIGHT,
  CARD_WIDTH,
}
