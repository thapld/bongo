import mapData from './mapData'
import dummyData from './dummyData'
import api from './api'

export default {
  mapData, dummyData, api,
}
