const gender = [
  {
    value: 1,
    label: 'Nam',
  },
  {
    value: 0,
    label: 'Nữ',
  },
]

const drinkTypes = [
  {
    value: 1,
    label: 'Rượu',
  },
  {
    value: 2,
    label: 'Bia',
  },
]

const alcoholTypes = [
  {
    value: 12,
    label: 'Bordeaux',
  },
  {
    value: 12,
    label: 'Bourgogne',
  },
  {
    value: 12,
    label: 'Champagne',
  },
  {
    value: 12,
    label: 'Côte du Rhône',
  },
  {
    value: 15,
    label: 'Chardonnay',
  },
  {
    value: 15,
    label: 'Cabernet Sauvignon',
  },
  {
    value: 15,
    label: 'Merlot',
  },
  {
    value: 40,
    label: 'Hennessy',
  },
  {
    value: 40,
    label: 'Remy Martin',
  },
  {
    value: 40,
    label: 'Martel, Otard',
  },
  {
    value: 40,
    label: 'Courvoisier',
  },
  {
    value: 40,
    label: 'Camus',
  },
  {
    value: 40,
    label: 'Hines',
  },
  {
    value: 40,
    label: 'Vodka',
  },
  {
    value: 40,
    label: 'Lúa mới',
  },
  {
    value: 40,
    label: 'Nếp mới',
  },
]

const beerTypes = [
  {
    value: '330~5.3',
    label: '333',
  },
  {
    value: '330~5',
    label: 'Carlberg 330ml',
  },
  {
    value: '500~5',
    label: 'Carlberg 500ml',
  },
  {
    value: '330~4.6',
    label: 'Hà Nội 330ml (4.6%)',
  },
  {
    value: '330~5.1',
    label: 'Hà Nội 330ml (5.1%)',
  },
  {
    value: '450~4.2',
    label: 'Hà Nội 450ml',
  },
  {
    value: '250~5',
    label: 'Heineken 250ml',
  },
  {
    value: '330~5',
    label: 'Heineken 330ml',
  },
  {
    value: '500~7',
    label: 'Heineken 500ml',
  },
  {
    value: '330~4.7',
    label: 'Huda 330ml',
  },
  {
    value: '350~4.7',
    label: 'Huda 350ml',
  },
  {
    value: '500~5',
    label: 'Huda 500ml',
  },
  {
    value: '330~4',
    label: 'Larue 330ml',
  },
  {
    value: '355~4',
    label: 'Larue 355ml',
  },
  {
    value: '355~4.9',
    label: 'Sài gòn Export',
  },
  {
    value: '450~4.3',
    label: 'Sài gòn Lager',
  },
  {
    value: '330~4.9',
    label: 'Sàn gòn Special',
  },
  {
    value: '330~5',
    label: 'Sam Miguel 330ml',
  },
  {
    value: '500~5',
    label: 'Sam Miguel 500ml',
  },
  {
    value: '330~5',
    label: 'Sapporo 330ml',
  },
  {
    value: '650~5.2',
    label: 'Sapporo 650ml',
  },
  {
    value: '330~5',
    label: 'Tiger 330ml',
  },
  {
    value: '500~5',
    label: 'Tiger 500ml',
  },
]

export default {
  gender,
  alcoholTypes,
  beerTypes,
  drinkTypes,
}
