import React from 'react'
import { StyleSheet } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'

const GOOGLE_MAP_KEY = 'AIzaSyB46eqmEIAssVc7gpvCaXHKmgbaHzIVoNU'

const styles = StyleSheet.create({
  chipsIcon: {
    marginRight: 5,
  },
})

const region = {
  latitude: 21.028511,
  longitude: 105.804817,
  latitudeDelta: 0.04864195044303443,
  longitudeDelta: 0.040142817690068,
}

const mapCategory = [
  {
    id: 1,
    name: 'Trạm xăng',
    icon: <MaterialCommunityIcons name="gas-station" size={18} style={styles.chipsIcon} />,
  },
  {
    id: 2,
    name: 'Sửa xe',
    icon: <MaterialCommunityIcons name="car-cog" size={18} style={styles.chipsIcon} />,
  },
  {
    id: 3,
    name: 'Rửa xe',
    icon: <MaterialCommunityIcons name="car-wash" size={18} style={styles.chipsIcon} />,
  },
  {
    id: 4,
    name: 'Điểm đỗ',
    icon: <MaterialCommunityIcons name="parking" size={18} style={styles.chipsIcon} />,
  },
  {
    id: 5,
    name: 'Ắc quy',
    icon: <MaterialCommunityIcons name="car-electric" size={18} style={styles.chipsIcon} />,
  },
  {
    id: 6,
    name: 'Làm lốp',
    icon: <MaterialCommunityIcons name="circle-slice-8" size={18} style={styles.chipsIcon} />,
  },
  {
    id: 7,
    name: 'Cứu hộ',
    icon: <MaterialCommunityIcons name="lifebuoy" size={18} style={styles.chipsIcon} />,
  },
]

const initialMapState = {
  markers: [],
  categories: mapCategory,
  region,
}

export default {
  mapCategory, region, initialMapState, GOOGLE_MAP_KEY,
}
