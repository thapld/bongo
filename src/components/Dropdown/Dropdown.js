import React, { useEffect, useRef, useState } from 'react'
import {
  FlatList,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import { colors, fonts, sizes } from '../../theme'

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.grayx,
    height: 50,
    zIndex: 1,
  },
  buttonText: {
    flex: 1,
    textAlign: 'center',
    color: colors.gray,
    ...fonts.body4,
  },
  icon: {
    marginRight: 0,
    color: colors.gray,
  },
  dropdown: {
    position: 'absolute',
    backgroundColor: colors.white,
    width: '100%',
    shadowColor: '#000000',
    shadowRadius: 4,
    shadowOffset: { height: 4, width: 0 },
    shadowOpacity: 0.5,
  },
  overlay: {
    width: '100%',
    height: '100%',
  },
  item: {
    paddingVertical: sizes.radius,
    backgroundColor: colors.transparent,
  },
})

const Dropdown = ({
  label,
  data,
  onSelect,
  containerStyle,
  errorMsg,
  placeholder,
  defaultValue, panelStyle,
}) => {
  const DropdownButton = useRef()
  const [visible, setVisible] = useState(false)
  const [selected, setSelected] = useState(undefined)
  const [dropdownTop, setDropdownTop] = useState(0)

  const openDropdown = () => {
    DropdownButton.current.measure((_fx, _fy, _w, h, _px, py) => {
      setDropdownTop(py + h)
    })
    setVisible(true)
  }

  const toggleDropdown = () => (visible ? setVisible(false) : openDropdown())

  const onItemPress = (item) => {
    setSelected(item)
    onSelect(item.value)
    setVisible(false)
  }

  const renderItem = ({ item }) => (
    <TouchableOpacity style={styles.item} onPress={() => onItemPress(item)}>
      <Text
        style={{
          paddingHorizontal: sizes.radius,
          color: colors.gray,
          ...fonts.body4,
        }}
      >
        {item.label}
      </Text>
    </TouchableOpacity>
  )

  const renderDropdown = () => (
    <Modal visible={visible} transparent animationType="none">
      <TouchableOpacity
        style={styles.overlay}
        onPress={() => setVisible(false)}
      >
        <View style={[{ top: dropdownTop, height: 300 }]}>
          <FlatList
            data={data}
            renderItem={renderItem}
            showsVerticalScrollIndicator
            contentContainerStyle={{
              marginTop: sizes.base,
              backgroundColor: colors.lightGray1,
              marginHorizontal: sizes.radius,
              borderRadius: sizes.radius,
              ...panelStyle,
            }}
            bounces={false}
            keyExtractor={(item, index) => `${item.value}-${index}`}
          />
        </View>
      </TouchableOpacity>
    </Modal>
  )

  useEffect(() => {
    if (defaultValue && defaultValue.value) {
      setSelected(defaultValue)
      onSelect(defaultValue.value)
    }
  }, [defaultValue])

  return (
    <View
      style={{
        ...containerStyle,
      }}
    >
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}
      >
        <Text style={{ color: colors.gray, ...fonts.body4 }}>{label}</Text>
        <Text style={{ color: colors.red, ...fonts.body4 }}>{errorMsg}</Text>
      </View>
      <TouchableOpacity
        ref={DropdownButton}
        style={[
          styles.button,
          {
            flexDirection: 'row',
            height: 55,
            paddingHorizontal: sizes.padding,
            marginTop: sizes.base,
            borderRadius: sizes.radius,
            backgroundColor: colors.lightGray1,
            justifyContent: 'space-between',
            alignItems: 'center',
          },
        ]}
        onPress={toggleDropdown}
      >
        {renderDropdown()}
        <Text style={styles.buttonText} placeholder={placeholder}>
          {(selected && selected.label) || ''}
        </Text>
        <FontAwesome name="chevron-down" style={styles.icon} />
      </TouchableOpacity>
    </View>
  )
}

export default Dropdown
