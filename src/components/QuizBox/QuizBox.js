import React, { useState } from 'react'
import {
  FlatList, Text, TouchableOpacity, View,
} from 'react-native'
import dummyData from '../../constants/dummyData'
import {
  colors, fonts, globalStyles, sizes,
} from '../../theme'
import TextButton from '../TextButton'

const QuizBox = () => {
  const [active, setActive] = useState(-1)
  const [status, setStatus] = useState(false)

  return (
    <View style={[globalStyles.shadow, {
      flex: 1,
      marginLeft: sizes.padding,
      marginRight: sizes.padding,
      backgroundColor: colors.white,
      borderRadius: 10,
      padding: sizes.radius,
    }]}
    >
      <View style={{ flex: 1, marginTop: sizes.radius }}>
        <Text style={{ ...fonts.h4 }}>{dummyData.quiz.question}</Text>
      </View>
      <FlatList
        style={{
          marginVertical: sizes.radius,
        }}
        data={dummyData.quiz.answers}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            key={`qb-${index}`}
            style={{
              flex: 1,
              flexDirection: 'row',
              marginVertical: sizes.radius,
            }}
            onPress={() => {
              setActive(index)
              if (index === 1) {
                setStatus(true)
              } else {
                setStatus(false)
              }
            }}
          >
            <View style={[globalStyles.center, {
              flex: 1,
            }]}
            >
              <View style={{
                backgroundColor: active === index ? (status ? colors.green : colors.pink) : colors.transparent,
                height: 20,
                width: 20,
                borderWidth: 1,
                borderRadius: 10,
                borderColor: colors.gray,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              >
                <Text style={{ color: active === index ? colors.white : colors.black }}>{item.label}</Text>
              </View>
            </View>
            <View style={[globalStyles.center, { flex: 15, alignItems: 'flex-start' }]}>
              <Text style={{ marginLeft: 10 }}>{item.text}</Text>
            </View>
          </TouchableOpacity>
        )}
        ListFooterComponent={(
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-around',
            }}
          >
            <TextButton
              label="Đáp án"
              labelStyle={{
                color: colors.lightPurple,
                ...fonts.h4,
              }}
              buttonContainerStyle={{
                backgroundColor: colors.transparent,
              }}
            />
            <TextButton
              label="Câu khác"
              labelStyle={{
                color: colors.lightPurple,
                ...fonts.h4,
              }}
              buttonContainerStyle={{
                backgroundColor: colors.transparent,
                marginHorizontal: sizes.radius,
              }}
            />
          </View>
        )}
      />
    </View>
  )
}

export default QuizBox
