import React from 'react'
import {
  Image, StyleSheet, Text, TouchableOpacity, View,
} from 'react-native'
import { colors, fonts, sizes } from '../../theme'

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
})

const SignalVerticalCard = ({ item, onPress, containerStyle }) => (
  <TouchableOpacity
    style={[styles.shadow, {
      width: 150,
      padding: sizes.radius,
      alignItems: 'center',
      borderRadius: sizes.radius,
      backgroundColor: colors.white,
      ...containerStyle,
    }]}
    onPress={onPress}
  >
    <View
      style={{
        height: 150,
        width: 150,
        alignItems: 'center',
        justifyContent: 'space-around',
      }}
    >
      <Image
        source={item.image}
        style={{
          height: 100,
          width: 100,
          resizeMode: 'contain',
        }}
      />
      <Text style={{
        color: colors.darkGray2,
        textAlign: 'center',
        paddingHorizontal: 5,
        ...fonts.body5,
      }}
      >
        {item.title}
      </Text>
    </View>
  </TouchableOpacity>
)

export default SignalVerticalCard
