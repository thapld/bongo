import React from 'react'
import { Image, TouchableOpacity } from 'react-native'

const IconButton = ({
  containerStyle,
  icon,
  iconStyle,
  onPress,
}) => (
  <TouchableOpacity
    style={{
      ...containerStyle,
    }}
    onPress={onPress}
  >
    <Image
      source={icon}
      style={{
        width: 30,
        height: 30,
        ...iconStyle,
      }}
    />
  </TouchableOpacity>
)

export default IconButton
