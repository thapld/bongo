import React from 'react'
import { StyleSheet, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { Header } from '@react-navigation/stack'
import { colors } from '../../theme'

const GradientHeader = (props) => (
  <View style={{ backgroundColor: colors.darkPurple }}>
    <LinearGradient
      colors={[colors.darkPurple, colors.lightPurple]}
      style={[StyleSheet.absoluteFill, { height: 100 }]}
    >
      <Header {...props} />
    </LinearGradient>
  </View>
)

export default GradientHeader
