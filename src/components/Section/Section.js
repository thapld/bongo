import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import { colors, fonts, sizes } from '../../theme'

const Section = ({
  title, onPress, children, hasTitle = true,
  hasAction = true, actionLabel = 'Show All',
  actionIcon = false, icon, headerContainerStyle,
  actionLabelStyle,
}) => (
  <View>
    {/* Header */}
    <View
      style={{
        flexDirection: 'row',
        marginHorizontal: sizes.padding,
        marginVertical: 15,
        ...headerContainerStyle,
      }}
    >
      {
        hasTitle ? (
          <Text style={{ flex: 1, ...fonts.h3 }}>
            {title}
          </Text>
        ) : null
      }
      {
        hasAction ? (
          <TouchableOpacity
            onPress={onPress}
          >
            {
              actionIcon ? icon
                : (
                  <Text style={{ color: colors.lightPurple, ...fonts.body4, ...actionLabelStyle }}>
                    {actionLabel}
                  </Text>
                )
            }
          </TouchableOpacity>
        ) : null
      }
    </View>

    {/* Content */}
    {children}
  </View>
)

export default Section
