import React from 'react'
import { Image, Text, TouchableOpacity } from 'react-native'
import { fonts } from '../../theme'

const TextIconButton = ({
  containerStyle,
  label,
  labelStyle,
  iconStyle,
  icon,
  onPress,
  iconPosition = 'LEFT',
}) => (
  <TouchableOpacity
    style={{
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      ...containerStyle,
    }}
    onPress={onPress}
  >
    {iconPosition === 'LEFT' && (
      <Image
        source={icon}
        style={{
          marginLeft: 5,
          width: 24,
          height: 24,
          ...iconStyle,
        }}
      />
    )}
    <Text
      style={{
        ...fonts.body3,
        ...labelStyle,
      }}
    >
      {label}
    </Text>
    {iconPosition === 'RIGHT' && (
      <Image
        source={icon}
        style={{
          marginLeft: 5,
          width: 24,
          height: 24,
          ...iconStyle,
        }}
      />
    )}
  </TouchableOpacity>
)

export default TextIconButton
