import React from 'react'
import { Text, View } from 'react-native'
import { colors, fonts, sizes } from '../../theme'

const DualText = ({
  text1, text2, text1Style, text2Style, containerStyle,
}) => (
  <View
    style={{
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: colors.lightGray2,
      paddingHorizontal: sizes.radius,
      paddingVertical: sizes.radius,
      borderRadius: sizes.radius,
      ...containerStyle,
    }}
  >
    <Text style={{ ...text1Style, ...fonts.h3 }}>{text1}</Text>
    <Text
      style={{
        ...text2Style,
        flexShrink: 1,
        marginLeft: sizes.padding,
      }}
    >
      {text2}
    </Text>
  </View>
)

export default DualText
