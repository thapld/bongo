import React from 'react'
import {
  Image, Text, TouchableOpacity, View,
} from 'react-native'
import { fonts } from '../../theme'

const TitleIconButton = ({
  containerStyle, buttonContainerStyle, label, labelStyle, iconStyle, icon, onPress,
}) => (
  <View style={{
    flex: 1,
    width: 50,
    height: 50,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    ...containerStyle,
  }}
  >
    <TouchableOpacity
      style={{
        ...buttonContainerStyle,
      }}
      onPress={onPress}
    >
      <Image
        source={icon}
        style={{
          marginLeft: 5,
          width: 24,
          height: 24,
          ...iconStyle,
        }}
      />
    </TouchableOpacity>
    <Text
      style={{
        ...fonts.body4,
        ...labelStyle,
      }}
    >
      {label}
    </Text>
  </View>
)

export default TitleIconButton
