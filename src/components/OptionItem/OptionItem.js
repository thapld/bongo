import React from 'react'
import {
  ActivityIndicator,
  Image, StyleSheet, Text, TouchableOpacity, View,
} from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { colors, fonts, sizes } from '../../theme'

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
})

const OptionItem = ({
  icon, bgColor, label, onPress,
  source, iconType,
  containerStyle,
  gradientStyle,
  imageStyle,
  loading = false,
}) => (
  <TouchableOpacity
    style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
    onPress={onPress}
  >
    <View style={[styles.shadow, { width: 40, height: 40, ...containerStyle }]}>
      <LinearGradient
        colors={bgColor}
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: 8,
          ...gradientStyle,
        }}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
      >
        {
          loading ? (<ActivityIndicator size="small" color={colors.lightPurple} />)
            : (
              iconType === true ? icon
                : (
                  <Image
                    source={source}
                    resizeMode="cover"
                    style={{
                      width: 28,
                      height: 28,
                      ...imageStyle,
                    }}
                  />
                )
            )
        }
      </LinearGradient>
    </View>
    {
      label ? <Text style={{ marginTop: sizes.base, color: colors.gray, ...fonts.body5 }}>{label}</Text> : null
    }
  </TouchableOpacity>
)

export default OptionItem
