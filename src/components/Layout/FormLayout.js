import React from 'react'
import { ScrollView } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { colors, sizes } from '../../theme'

const FormLayout = ({ children }) => (
  <ScrollView
    showsVerticalScrollIndicator={false}
    style={{
      flex: 1,
      backgroundColor: colors.white,
      paddingVertical: sizes.padding,
    }}
  >
    <KeyboardAwareScrollView
      contentContainerStyle={{
        flex: 1,
        paddingHorizontal: sizes.padding,
      }}
    >
      {/* Content/ Children */}
      {children}
    </KeyboardAwareScrollView>
  </ScrollView>
)

export default FormLayout
