import React from 'react'
import { Text, TextInput, View } from 'react-native'
import { colors, fonts, sizes } from '../../theme'

const FormInput = ({
  containerStyle,
  label,
  placeholder,
  inputStyle,
  prependComponent,
  appendComponent,
  onChange,
  secureTextEntry,
  keyboardType = 'default',
  autoCompleteType = 'off',
  autoCapitalize = 'none',
  errorMsg = '',
}) => (
  <View
    style={{
      ...containerStyle,
    }}
  >
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}
    >
      <Text style={{ color: colors.gray, ...fonts.body4 }}>{label}</Text>
      <Text style={{ color: colors.red, ...fonts.body4 }}>{errorMsg}</Text>
    </View>

    {/* Text input */}
    <View
      style={{
        flexDirection: 'row',
        height: 55,
        paddingHorizontal: sizes.padding,
        marginTop: sizes.base,
        borderRadius: sizes.radius,
        backgroundColor: colors.lightGray1,
        borderWidth: errorMsg ? 1 : 0,
        borderColor: colors.red,
      }}
    >
      {prependComponent}

      <TextInput
        style={{
          flex: 1,
          ...inputStyle,
        }}
        placeholder={placeholder}
        placeholderTextColor={colors.gray2}
        secureTextEntry={secureTextEntry}
        keyboardType={keyboardType}
        autoCompleteType={autoCompleteType}
        autoCapitalize={autoCapitalize}
        onChangeText={(text) => onChange(text)}
        clearButtonMode="always"
      />

      {appendComponent}
    </View>
  </View>
)

export default FormInput
