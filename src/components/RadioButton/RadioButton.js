import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { colors, fonts, sizes } from '../../theme'

const RadioButton = ({
  containerStyle,
  label,
  labelStyle,
  isSelected,
  onPress,
}) => (
  <TouchableOpacity
    style={{
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      ...containerStyle,
    }}
    onPress={onPress}
  >
    {isSelected ? (
      <MaterialIcons name="radio-button-checked" size={24} />
    ) : (
      <MaterialIcons name="radio-button-unchecked" size={24} />
    )}

    <Text
      style={{
        marginLeft: sizes.radius,
        color: colors.gray,
        ...fonts.body3,
        ...labelStyle,
      }}
    >
      {label}
    </Text>
  </TouchableOpacity>
)

export default RadioButton
