import React from 'react'
import { Image, Text, View } from 'react-native'
import { sizes, fonts } from '../theme'

const IconLabel = ({
  containerStyle,
  icon,
  iconStyle,
  label,
  labelStyle,
  imageType = true,
  title,
  titleStyle,
}) => (
  <View
    style={{
      flexDirection: 'row',
      paddingVertical: sizes.base,
      paddingHorizontal: sizes.radius,
      borderRadius: sizes.radius,
      alignItems: 'center',
      ...containerStyle,
    }}
  >
    {
      icon ? (
        imageType ? (
          <Image
            source={icon}
            style={{
              width: 20,
              height: 20,
              ...iconStyle,
            }}
          />
        ) : (
          icon
        )
      ) : null
    }
    {title ? (
      <View style={{
        flexDirection: 'row',
      }}
      >
        <Text
          style={{
            flexShrink: 1,
            marginLeft: sizes.base,
            ...fonts.body3,
            ...titleStyle,
          }}
        >
          {title}
        </Text>
      </View>
    ) : null}
    <Text
      style={{
        marginLeft: sizes.base,
        ...fonts.body3,
        ...labelStyle,
      }}
    >
      {label}
    </Text>
  </View>
)

export default IconLabel
