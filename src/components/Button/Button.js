import React from 'react'
import {
  TouchableOpacity, Text, View, ActivityIndicator,
} from 'react-native'
import { colors, fonts, sizes } from '../../theme'

const Button = ({
  disabled,
  containerStyle,
  label,
  labelStyle,
  onPress,
  loading,
}) => (
  <View
    style={{
      opacity: disabled ? 0.5 : 1,
    }}
    pointerEvents={disabled ? 'none' : 'auto'}
  >
    <TouchableOpacity
      style={{
        backgroundColor: colors.lightPurple,
        padding: sizes.radius,
        borderRadius: sizes.radius,
        alignItems: 'center',
        justifyContent: 'center',
        height: 55,
        ...containerStyle,
      }}
      onPress={onPress}
    >
      {loading ? (
        <ActivityIndicator size="small" color={colors.white} />
      ) : (
        <Text style={{ color: colors.white, ...fonts.h3, ...labelStyle }}>
          {label}
        </Text>
      )}
    </TouchableOpacity>
  </View>
)

export default Button
