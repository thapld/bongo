import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { colors, fonts } from '../theme'

const TextButton = ({
  label, labelStyle, buttonContainerStyle,
  onPress, disabled = false,
  label2 = '',
  label2Style = '',
}) => (
  <TouchableOpacity
    disabled={disabled}
    style={{
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: colors.lightGrayPurple,
      ...buttonContainerStyle,
    }}
    onPress={onPress}
  >
    <Text style={{
      color: colors.white,
      ...fonts.h3,
      ...labelStyle,
    }}
    >
      {label}
    </Text>

    {
                label2 !== '' && (
                <Text style={{
                  flex: 1,
                  textAlign: 'right',
                  color: colors.white,
                  ...fonts.h3,
                  ...label2Style,
                }}
                >
                  {label2}
                </Text>
                )
}
  </TouchableOpacity>
)

export default TextButton
