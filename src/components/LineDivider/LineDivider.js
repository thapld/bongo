import React from 'react'
import { View } from 'react-native'
import { colors } from '../../theme'

const LineDivider = ({ lineStyle }) => (
  <View
    style={{
      height: 2,
      width: '100%',
      backgroundColor: colors.lightGray2,
      ...lineStyle,
    }}
  />
)

export default LineDivider
