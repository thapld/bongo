import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { colors, fonts, sizes } from '../../theme'

const Checkbox = ({
  containerStyle,
  checkboxBlankStyle,
  checkboxStyle,
  checked,
  label,
  labelStyle,
  size = 24,
  color = colors.lightPurple,
  onPress,
}) => (
  <View
    style={{
      flex: 1,
      flexDirection: 'row',
      ...containerStyle,
    }}
  >
    {checked ? (
      <TouchableOpacity onPress={() => onPress(false)}>
        <MaterialIcons
          size={size}
          color={color}
          style={{ ...checkboxStyle }}
          name="check-box"
        />
      </TouchableOpacity>
    ) : (
      <TouchableOpacity onPress={() => onPress(true)}>
        <MaterialIcons
          size={24}
          color={color}
          name="check-box-outline-blank"
          style={{ ...checkboxBlankStyle }}
        />
      </TouchableOpacity>
    )}
    <Text
      style={{
        marginLeft: sizes.base,
        ...fonts.body4,
        color: colors.gray,
        ...labelStyle,
      }}
    >
      {label}
    </Text>
  </View>
)

export default Checkbox
