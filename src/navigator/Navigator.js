import React, { useEffect } from 'react'
import { StyleSheet, Text } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { authenticate } from 'slices/app.slice'

import { createNativeStackNavigator } from 'react-native-screens/native-stack'
import { NavigationContainer } from '@react-navigation/native'
import TabNavigator from './Tabs'
import Place from '../pages/Place'
import PhatNguoi from '../pages/PhatNguoi'
import PNHeaderLeft from '../pages/PhatNguoi/PNHeaderLeft'
import PNHeaderRight from '../pages/PhatNguoi/PNHeaderRight'
import { colors } from '../theme'
import NongDoCon from '../pages/NongDoCon'
import PhongThuy from '../pages/PhongThuy'
import BienSoXe from '../pages/BienSoXe/BienSoXe'
import BienSoxeDistrict from '../pages/BienSoXe/BienSoxeDistrict'
import BienSoXeSpec from '../pages/BienSoXe/BienSoXeSpec'

const Stack = createNativeStackNavigator()

const navigationProps = {
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: colors.lightPurple,
  },
  headerTitleStyle: { fontSize: 18 },
}

const styles = StyleSheet.create({
  headerText: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
})

const Navigator = () => {
  const { checked, loggedIn } = useSelector((state) => state.app)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(authenticate({ loggedIn: true, checked: true }))
  }, [])

  // TODO: switch router by loggedIn state
  console.log('[##] loggedIn', loggedIn)

  return checked ? (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          options={{ headerShown: false }}
          component={TabNavigator}
        />
        <Stack.Screen
          name="Place"
          options={{ headerShown: false }}
          component={Place}
        />
        <Stack.Screen
          name="PhatNguoi"
          component={PhatNguoi}
          options={(props) => ({
            headerLeft: () => <PNHeaderLeft navigation={props.navigation} />,
            headerCenter: () => (
              <Text style={styles.headerText}>Phạt nguội</Text>
            ),
            headerRight: () => <PNHeaderRight />,
            ...navigationProps,
          })}
        />
        <Stack.Screen
          name="NongDoCon"
          component={NongDoCon}
          options={(props) => ({
            headerLeft: () => <PNHeaderLeft navigation={props.navigation} />,
            headerCenter: () => (
              <Text style={styles.headerText}>Nồng độ cồn</Text>
            ),
            ...navigationProps,
          })}
        />
        <Stack.Screen
          name="PhongThuy"
          component={PhongThuy}
          options={(props) => ({
            headerLeft: () => <PNHeaderLeft navigation={props.navigation} />,
            headerCenter: () => (
              <Text style={styles.headerText}>Phong thủy</Text>
            ),
            ...navigationProps,
          })}
        />
        <Stack.Screen
          name="BienSoXe"
          component={BienSoXe}
          options={(props) => ({
            headerLeft: () => <PNHeaderLeft navigation={props.navigation} />,
            headerCenter: () => (
              <Text style={styles.headerText}>Biển số xe</Text>
            ),
            ...navigationProps,
          })}
        />
        <Stack.Screen
          name="BienSoXeDistrict"
          component={BienSoxeDistrict}
          options={(props) => ({
            headerLeft: () => <PNHeaderLeft navigation={props.navigation} />,
            headerCenter: () => (
              <Text style={styles.headerText}>
                Biển số xe {props.route.params.name}
              </Text>
            ),
            ...navigationProps,
          })}
        />
        <Stack.Screen
          name="BienSoXeSpec"
          component={BienSoXeSpec}
          options={(props) => ({
            headerLeft: () => <PNHeaderLeft navigation={props.navigation} />,
            headerCenter: () => (
              <Text style={styles.headerText}>
                {props.route.params.item.title}
              </Text>
            ),
            ...navigationProps,
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  ) : (
    <Text>Loading...</Text>
  )
}

export default Navigator
