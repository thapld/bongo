import React from 'react'
import { View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import FontIcon from 'react-native-vector-icons/FontAwesome5'
import AntDesign from 'react-native-vector-icons/AntDesign'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Octicons from 'react-native-vector-icons/Octicons'
import { colors } from 'theme'

// stack navigators
import {
  HomeNavigator,
  CompareNavigator,
  NewsNavigator,
  SearchNavigator,
} from '../Stacks'

const Tab = createBottomTabNavigator()

const TabNavigator = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ focused }) => {
        switch (route.name) {
          case 'Home':
            return (
              <FontIcon
                name="home"
                color={focused ? colors.lightPurple : colors.gray}
                size={20}
                solid
              />
            )
          case 'Learn':
            return (
              <AntDesign
                name="book"
                color={focused ? colors.lightPurple : colors.gray}
                size={20}
                solid
              />
            )
          case 'Compare':
            return (
              <Octicons
                name="git-compare"
                color={focused ? colors.lightPurple : colors.gray}
                size={20}
                solid
              />
            )
          case 'News':
            return (
              <FontAwesome
                name="newspaper-o"
                color={focused ? colors.lightPurple : colors.gray}
                size={20}
                solid
              />
            )
          case 'Search':
            return (
              <FontIcon
                name="search"
                color={focused ? colors.lightPurple : colors.gray}
                size={20}
                solid
              />
            )
          default:
            return <View />
        }
      },
    })}
    tabBarOptions={{
      activeTintColor: colors.lightPurple,
      inactiveTintColor: colors.gray,
      style: {
        // backgroundColor: 'white',
        // borderTopColor: 'gray',
        // borderTopWidth: 1,
        // paddingBottom: 5,
        // paddingTop: 5,
      },
    }}
    initialRouteName="Home"
    swipeEnabled={false}
  >
    <Tab.Screen
      name="Home"
      options={{ title: 'Trang chủ' }}
      component={HomeNavigator}
    />
    <Tab.Screen
      name="Learn"
      options={{ title: 'Học tập' }}
      component={HomeNavigator}
    />
    <Tab.Screen
      name="News"
      options={{ title: 'Tin tức' }}
      component={NewsNavigator}
    />
    <Tab.Screen
      name="Compare"
      options={{ title: 'So sánh' }}
      component={CompareNavigator}
    />
    <Tab.Screen
      name="Search"
      options={{ title: 'Tra cứu' }}
      component={SearchNavigator}
    />
  </Tab.Navigator>
)

export default TabNavigator
