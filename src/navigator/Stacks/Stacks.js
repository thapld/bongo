import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Home from 'pages/Home'
import GradientHeader from '../../components/GradientHeader'
import { colors } from '../../theme'
import News from '../../pages/News'

// ------------------------------------
// Constants
// ------------------------------------

const Stack = createStackNavigator()

const navigationProps = {
  headerTintColor: 'white',
  header: (props) => <GradientHeader {...props} />,
  headerStyle: {
    backgroundColor: colors.transparent,
  },
  headerTitleStyle: { fontSize: 18 },
}

// ------------------------------------
// Navigators
// ------------------------------------

export const HomeNavigator = () => (
  <Stack.Navigator
    initialRouteName="Home"
    headerMode="screen"
    screenOptions={navigationProps}
  >
    <Stack.Screen
      name="Home"
      component={Home}
      options={() => ({
        headerShown: false,
        title: '',
      })}
    />
  </Stack.Navigator>
)

export const NewsNavigator = () => (
  <Stack.Navigator
    initialRouteName="News"
    headerMode="screen"
    screenOptions={navigationProps}
  >
    <Stack.Screen
      name="News"
      component={News}
      options={() => ({
        headerShown: false,
        title: '',
      })}
    />
  </Stack.Navigator>
)

export const CompareNavigator = () => (
  <Stack.Navigator
    initialRouteName="Compare"
    headerMode="screen"
    screenOptions={navigationProps}
  >
    <Stack.Screen
      name="Compare"
      component={Home}
      options={() => ({
        headerShown: false,
        title: '',
      })}
    />
  </Stack.Navigator>
)

export const SearchNavigator = () => (
  <Stack.Navigator
    initialRouteName="Search"
    headerMode="screen"
    screenOptions={navigationProps}
  >
    <Stack.Screen
      name="Search"
      component={Home}
      options={() => ({
        headerShown: false,
        title: '',
      })}
    />
  </Stack.Navigator>
)

export const LearnNavigator = () => (
  <Stack.Navigator
    initialRouteName="Learn"
    headerMode="screen"
    screenOptions={navigationProps}
  >
    <Stack.Screen
      name="Learn"
      component={Home}
      options={() => ({
        headerShown: false,
        title: '',
      })}
    />
  </Stack.Navigator>
)
