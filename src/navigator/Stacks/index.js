import {
  HomeNavigator, CompareNavigator, NewsNavigator, SearchNavigator, LearnNavigator,
} from './Stacks'

export {
  HomeNavigator, CompareNavigator, NewsNavigator, SearchNavigator, LearnNavigator,
}
