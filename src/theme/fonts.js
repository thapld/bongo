import * as Font from 'expo-font'
import sizes from './sizes'

// fonts preloading
export const fontAssets = [
  {
    openSans_regular: require('../../assets/fonts/OpenSans-Regular.ttf'),
  },
  {
    openSans_regular_italic: require('../../assets/fonts/OpenSans-Italic.ttf'),
  },
  {
    openSans_semiBold: require('../../assets/fonts/OpenSans-Semibold.ttf'),
  },
  {
    openSans_semiBold_italic: require('../../assets/fonts/OpenSans-SemiboldItalic.ttf'),
  },
  {
    openSans_bold: require('../../assets/fonts/OpenSans-Bold.ttf'),
  },
  {
    openSans_bold_italic: require('../../assets/fonts/OpenSans-BoldItalic.ttf'),
  },
].map((x) => Font.loadAsync(x))

const fonts = {
  openSan: {
    regular: 'openSans_regular',
    regularItalic: 'openSans_regular_italic',
    semiBold: 'openSans_semiBold',
    semiBoldItalic: 'openSans_semiBold_italic',
    bold: 'openSans_bold',
    boldItalic: 'openSans_bold_italic',
  },
  largeTitle: { fontFamily: 'openSans_bold', fontSize: sizes.largeTitle },
  h1: { fontFamily: 'openSans_bold', fontSize: sizes.h1, lineHeight: 36 },
  h2: { fontFamily: 'openSans_bold', fontSize: sizes.h2, lineHeight: 30 },
  h3: { fontFamily: 'openSans_semiBold', fontSize: sizes.h3, lineHeight: 22 },
  h4: { fontFamily: 'openSans_semiBold', fontSize: sizes.h4, lineHeight: 22 },
  h5: { fontFamily: 'openSans_semiBold', fontSize: sizes.h5, lineHeight: 22 },
  body1: { fontFamily: 'openSans_regular', fontSize: sizes.body1, lineHeight: 36 },
  body2: { fontFamily: 'openSans_regular', fontSize: sizes.body2, lineHeight: 30 },
  body3: { fontFamily: 'openSans_regular', fontSize: sizes.body3, lineHeight: 22 },
  body4: { fontFamily: 'openSans_regular', fontSize: sizes.body4, lineHeight: 22 },
  body5: { fontFamily: 'openSans_regular', fontSize: sizes.body5, lineHeight: 22 },
}

export default fonts
