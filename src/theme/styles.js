import { StyleSheet } from 'react-native'
import colors from './colors'
import fonts from './fonts'

const globalStyles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  center: { flex: 1, alignItems: 'center', justifyContent: 'center' },
  labelStyle: { color: colors.gray, ...fonts.body4 },
})

export default globalStyles
