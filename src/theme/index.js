import colors from './colors'
import fonts from './fonts'
import images from './images'
import sizes from './sizes'
import globalStyles from './styles'

export {
  colors, fonts, images, sizes, globalStyles,
}
