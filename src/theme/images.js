import { Asset } from 'expo-asset'

const images = {
  logo_sm: require('../../assets/images/logo-sm.png'),
  logo_lg: require('../../assets/images/logo-lg.png'),
  learning: require('../../assets/images/learning-2x.png'),
  license_plate: require('../../assets/images/license-plate.png'),
  driver_license: require('../../assets/images/driver-license.png'),
  star: require('../../assets/images/star.png'),
}

// image preloading
export const imageAssets = Object.keys(images).map((key) => Asset.fromModule(images[key]).downloadAsync())

export default images
